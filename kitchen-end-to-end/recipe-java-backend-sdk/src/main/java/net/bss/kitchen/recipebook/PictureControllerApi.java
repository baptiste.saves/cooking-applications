package net.bss.kitchen.recipebook;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;

import java.util.UUID;

/**
 * I do not know how to make generated retrofit2+gson supports the return type byte[]
 * So we get it old school
 */
public interface PictureControllerApi extends org.openapitools.client.api.PictureControllerApi {
    @GET("picture/{id}")
    Call<ResponseBody> workingGetPicture(
            @retrofit2.http.Path("id") UUID id
    );
}
