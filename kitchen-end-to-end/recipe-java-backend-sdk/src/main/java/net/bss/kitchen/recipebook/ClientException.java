package net.bss.kitchen.recipebook;

public class ClientException extends RuntimeException {
    public ClientException(String string) {
        super(string);
    }
}
