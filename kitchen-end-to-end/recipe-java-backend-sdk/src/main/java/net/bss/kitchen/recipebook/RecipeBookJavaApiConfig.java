package net.bss.kitchen.recipebook;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties("recipe-book-java")
@Data
public class RecipeBookJavaApiConfig {
    private String baseUrl;

}
