package net.bss.kitchen.recipebook;

import lombok.SneakyThrows;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import org.openapitools.client.ApiClient;
import org.openapitools.client.api.IngredientControllerApi;
import org.openapitools.client.api.RecipeControllerApi;
import org.openapitools.client.model.*;
import org.springframework.stereotype.Service;
import retrofit2.Response;

import java.io.File;
import java.util.List;
import java.util.Set;
import java.util.UUID;

@Service
public class RecipeBookClient {

    private final RecipeControllerApi recipeControllerApi;
    private final IngredientControllerApi ingredientControllerApi;
    private final PictureControllerApi pictureControllerApi;

    public RecipeBookClient(RecipeBookJavaApiConfig recipeBookJavaApiConfig) {
        var apiClient = new ApiClient();
        apiClient.getAdapterBuilder().baseUrl(recipeBookJavaApiConfig.getBaseUrl());
        recipeControllerApi = apiClient.createService(RecipeControllerApi.class);
        ingredientControllerApi = apiClient.createService(IngredientControllerApi.class);
        pictureControllerApi = apiClient.createService(PictureControllerApi.class);
    }

    @SneakyThrows
    public UUID createRecipe(CreateUpdateRecipeDto body) {
        var execute = recipeControllerApi.createRecipe(body).execute();
        assertSuccess(execute);
        return execute.body();
    }

    @SneakyThrows
    public UUID updateRecipe(UUID id, CreateUpdateRecipeDto body) {
        var execute = recipeControllerApi.updateRecipe(id, body).execute();
        assertSuccess(execute);
        return execute.body();
    }

    @SneakyThrows
    public RecipeDto getRecipe(UUID id) {
        var execute = recipeControllerApi.getRecipe(id).execute();
        assertSuccess(execute);
        return execute.body();
    }

    @SneakyThrows
    public List<RecipeListDto> getRecipes(Set<IngredientTypeEnum> typeContains,
                                          Set<IngredientTypeEnum> typeDoesNotContain) {
        var execute = recipeControllerApi.getRecipes(typeContains, typeDoesNotContain).execute();
        assertSuccess(execute);
        return execute.body();
    }

    @SneakyThrows
    public List<RecipeListDto> getRecipes() {
        var execute = recipeControllerApi.getRecipes(null, null).execute();
        assertSuccess(execute);
        return execute.body();
    }


    @SneakyThrows
    public IngredientDto getIngredient(UUID ingredientId) {
        var execute = ingredientControllerApi.getIngredient(ingredientId).execute();
        assertSuccess(execute);
        return execute.body();
    }

    @SneakyThrows
    public List<IngredientDto> getIngredients() {
        var execute = ingredientControllerApi.getIngredients(null).execute();
        assertSuccess(execute);
        return execute.body();
    }

    @SneakyThrows
    public UnitDto createUnit(CreateUnitDto createUnitDto) {
        var execute = ingredientControllerApi.createUnit(createUnitDto).execute();
        assertSuccess(execute);
        return execute.body();
    }

    @SneakyThrows
    @SuppressWarnings("java:S3740")
    private void assertSuccess(Response response) {
        if (!response.isSuccessful()) {
            throw new ClientException(response.errorBody().string());
        }
    }

    @SneakyThrows
    public List<UnitDto> getAllUnits() {
        var execute = ingredientControllerApi.getAllUnits().execute();
        assertSuccess(execute);
        return execute.body();
    }

    @SneakyThrows
    public Set<IngredientTypeEnum> getAllTypes() {
        var execute = ingredientControllerApi.getAllIngredientTypes().execute();
        assertSuccess(execute);
        return execute.body().getTypes();
    }

    @SneakyThrows
    public IngredientDto createIngredient(CreateIngredientDto createIngredientDto) {
        var execute = ingredientControllerApi.createIngredient(createIngredientDto).execute();
        assertSuccess(execute);
        return execute.body();
    }

    @SneakyThrows
    public IngredientDto replaceIngredientUnits(UUID id, UpdateIngredientUnitsDto updateIngredientUnitsDto) {
        var execute = ingredientControllerApi.replaceIngredientUnits(id, updateIngredientUnitsDto).execute();
        assertSuccess(execute);
        return execute.body();
    }

    @SneakyThrows
    public IngredientDto replaceIngredientTypes(UUID id, UpdateIngredientTypesDto updateIngredientTypesDto) {
        var execute = ingredientControllerApi.replaceIngredientTypes(id, updateIngredientTypesDto).execute();
        assertSuccess(execute);
        return execute.body();
    }


    @SneakyThrows
    public UUID uploadPicture(File file) {
        var body = RequestBody.create(file, MediaType.parse("application/octet-stream"));

        MultipartBody.Part filePart = MultipartBody.Part.createFormData("picture", file.getName(), body);

        var execute = pictureControllerApi.uploadPicture(filePart).execute();
        assertSuccess(execute);
        return execute.body();
    }

    @SneakyThrows
    public byte[] downloadPicture(UUID id) {
        var execute = pictureControllerApi.workingGetPicture(id)
                .execute();
        assertSuccess(execute);
        return execute.body().bytes();
    }
}
