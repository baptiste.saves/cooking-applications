Feature: I want to be able to manage ingredients

  Scenario: I can create a new unit
    When I create a new unit
    Then The create unit response has all the data
    Then I can retrieve the unit in the list

  Scenario: I can create an ingredient
    When I create a new ingredient
    Then The create ingredient response has all the data
    Then I can retrieve the ingredient in the list
    Then I can retrieve the ingredient by its id

  Scenario: I can update units on an ingredient
    Given I create a new ingredient
    When I update units
    Then The ingredient should be updated with new units

  Scenario: I can update types on an ingredient
    Given I create a new ingredient
    When I update types
    Then The ingredient should be updated with new types