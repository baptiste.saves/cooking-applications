Feature: I want to be able to manage pictures

  Scenario: I can upload a new picture
    When I upload a picture
    Then The answer is the id

  Scenario: I can download a new picture
    Given I upload a picture
    When I download the picture
    Then The picture has the same data