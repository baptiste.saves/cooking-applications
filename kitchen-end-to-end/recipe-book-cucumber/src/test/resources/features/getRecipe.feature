Feature: I can display a single recipe

  Scenario: I display a recipe
    Given I create a new recipe
    When I display the recipe
    Then I should see the same data as the created recipe