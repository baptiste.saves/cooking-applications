Feature: I can display a list of recipes

  Scenario: I find a newly created recipe
    Given I create a new recipe
    Given I create a new recipe
    When I get the list of recipes
    Then I should find the created recipes

  Scenario: I can filter recipes on type
    Given I create a new recipe with BEEF
    Given I create a new recipe with FISH
    When I get the list of recipes filtering on contains BEEF
    Then I should find the recipe with BEEF only

  Scenario: I can filter recipes on type
    Given I create a new recipe with BEEF
    Given I create a new recipe with FISH
    When I get the list of recipes filtering on does not contain FISH
    Then I should find the recipe with BEEF only