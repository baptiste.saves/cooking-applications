Feature: I want to update a recipe

  Scenario: I want to update a recipe
    Given I write a new recipe
    Given I submit the creation
    Given I update the recipe
    When I display the recipe
    Then I should see the same data as the created recipe