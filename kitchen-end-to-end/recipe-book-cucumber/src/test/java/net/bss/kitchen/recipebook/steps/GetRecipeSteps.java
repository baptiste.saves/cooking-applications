package net.bss.kitchen.recipebook.steps;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import lombok.SneakyThrows;
import net.bss.kitchen.recipebook.RecipeBookClient;
import net.bss.kitchen.recipebook.World;
import org.assertj.core.groups.Tuple;
import org.openapitools.client.model.MeasureDto;
import org.openapitools.client.model.RecipeDto;
import org.openapitools.client.model.RecipeListDto;

import java.util.Set;

import static java.time.LocalDate.now;
import static org.assertj.core.api.Assertions.assertThat;
import static org.openapitools.client.model.IngredientTypeEnum.BEEF;
import static org.openapitools.client.model.IngredientTypeEnum.FISH;

public class GetRecipeSteps {
    private final World world;
    private final RecipeBookClient recipeBookClient;

    public GetRecipeSteps(World world, RecipeBookClient recipeBookClient) {
        this.world = world;
        this.recipeBookClient = recipeBookClient;
    }

    @When("I get the list of recipes")
    public void iGetTheListOfRecipes() {
        world.recipes = recipeBookClient.getRecipes();
    }

    @Then("I should find the created recipes")
    public void iShouldFindTheCreatedRecipes() {
        assertThat(world.recipes)
                .extracting(RecipeListDto::getId)
                .containsAll(world.createdRecipesId);

        assertThat(world.recipes)
                .extracting(RecipeListDto::getTitle, RecipeListDto::getCreatedAt)
                .containsAll(world.createdRecipes.stream()
                        .map(createRecipeDto -> Tuple.tuple(createRecipeDto.getTitle(), now()))
                        .toList());
    }

    @SneakyThrows
    @When("I display the recipe")
    public void iDisplayTheRecipe() {
        world.recipe = recipeBookClient.getRecipe(world.createdRecipesId.get(0));
    }

    @Then("I should see the same data as the created recipe")
    public void iShouldSeeTheSameDataAsTheCreatedRecipe() {

        assertThat(world.recipe).isNotNull()
                .returns(now(), RecipeDto::getCreatedAt)
                .usingRecursiveComparison()
                .ignoringFields("id", "createdAt", "measures")
                .isEqualTo(world.createRecipeWorld.createRecipeDto);

        assertThat(world.recipe.getMeasures())
                .hasSize(world.createRecipeWorld.createRecipeDto.getMeasures().size())
                .extracting(MeasureDto::getIngredient, MeasureDto::getIngredientId, MeasureDto::getUnit, MeasureDto::getUnitId, MeasureDto::getQuantity)
                .containsExactlyInAnyOrderElementsOf(
                        world.createRecipeWorld.createRecipeDto.getMeasures()
                                .stream().map(createMeasureDto -> {
                                    var ingredient = recipeBookClient.getIngredient(createMeasureDto.getIngredientId());
                                    var unitDto1 = ingredient.getUnits()
                                            .stream()
                                            .filter(unitDto -> unitDto.getId().equals(createMeasureDto.getUnitId()))
                                            .findFirst()
                                            .orElseThrow();
                                    return Tuple.tuple(
                                            ingredient.getName(),
                                            ingredient.getId(),
                                            unitDto1.getName(),
                                            unitDto1.getId(),
                                            createMeasureDto.getQuantity()
                                    );
                                }).toList()

                );


    }

    @When("I get the list of recipes filtering on contains BEEF")
    public void iGetTheListOfRecipesFilteringOnBEEF() {
        world.recipes = recipeBookClient.getRecipes(Set.of(BEEF), null);
    }


    @When("I get the list of recipes filtering on does not contain FISH")
    public void iGetTheListOfRecipesFilteringOnDoesNotContainFISH() {
        world.recipes = recipeBookClient.getRecipes(null, Set.of(FISH));
    }

    @Then("I should find the recipe with BEEF only")
    public void iShouldFindTheRecipeWithBEEFOnly() {
        assertThat(world.recipes).extracting(RecipeListDto::getId).contains(world.createdRecipesId.get(0));
        assertThat(world.recipes).extracting(RecipeListDto::getId).doesNotContain(world.createdRecipesId.get(1));
    }
}
