package net.bss.kitchen.recipebook.steps;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import net.bss.kitchen.recipebook.DtoFactory;
import net.bss.kitchen.recipebook.RecipeBookClient;
import net.bss.kitchen.recipebook.World;
import org.openapitools.client.model.IngredientTypeEnum;

import static org.assertj.core.api.Assertions.assertThat;

@AllArgsConstructor
public class CreateRecipeSteps {
    private final World world;

    private final RecipeBookClient recipeBookClient;

    private final PictureSteps pictureSteps;
    private final IngredientSteps ingredientSteps;

    @Given("I write a new recipe")
    public void iWriteANewRecipe() {
        pictureSteps.iUploadAPicture();
        world.createRecipeWorld.createRecipeDto = DtoFactory.createRecipeDto(world.pictureId);
    }

    @When("I submit the creation")
    @SneakyThrows
    public void iSubmitTheCreation() {
        var recipe = recipeBookClient.createRecipe(world.createRecipeWorld.createRecipeDto);

        world.createdRecipesId.add(recipe);
        world.createdRecipes.add(world.createRecipeWorld.createRecipeDto);
    }

    @Then("The recipe has been created")
    public void theRecipeHasBeenCreated() {
        assertThat(world.createdRecipesId.get(0)).isNotNull();
    }

    @Given("I create a new recipe")
    public void iCreateANewRecipe() {
        iWriteANewRecipe();
        iSubmitTheCreation();
        theRecipeHasBeenCreated();
    }

    @Given("^I create a new recipe with (.+)$")
    public void iCreateANewRecipeWith(IngredientTypeEnum type) {
        ingredientSteps.iCreateANewIngredient();
        ingredientSteps.iUpdateTypes(type);
        ingredientSteps.iUpdateUnits();
        pictureSteps.iUploadAPicture();

        world.createRecipeWorld.createRecipeDto = DtoFactory.createRecipeDto(world.pictureId,
                world.ingredientWorld.updatedIngredient);
        iSubmitTheCreation();
        theRecipeHasBeenCreated();
    }


}
