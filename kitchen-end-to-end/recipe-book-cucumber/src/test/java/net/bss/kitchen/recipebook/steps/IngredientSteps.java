package net.bss.kitchen.recipebook.steps;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import lombok.AllArgsConstructor;
import net.bss.kitchen.recipebook.RecipeBookClient;
import net.bss.kitchen.recipebook.World;
import org.apache.commons.lang3.RandomStringUtils;
import org.openapitools.client.model.*;

import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.openapitools.client.model.IngredientTypeEnum.BEEF;
import static org.openapitools.client.model.IngredientTypeEnum.CHICKEN;

@AllArgsConstructor
public class IngredientSteps {

    private final World world;
    private final RecipeBookClient recipeBookClient;

    @When("I create a new unit")
    public void iCreateAUnit() {
        var createUnitDto = new CreateUnitDto(RandomStringUtils.randomAlphanumeric(1, 15));
        world.ingredientWorld.createUnitDto = createUnitDto;
        world.ingredientWorld.unit = recipeBookClient.createUnit(
                createUnitDto);
    }

    @Then("I can retrieve the unit in the list")
    public void iCanRetrieveTheUnitInTheList() {
        assertThat(recipeBookClient.getAllUnits()).contains(world.ingredientWorld.unit);
    }

    @When("I create a new ingredient")
    public void iCreateANewIngredient() {
        var createIngredientDto = new CreateIngredientDto(RandomStringUtils.randomAlphanumeric(1, 10));
        world.ingredientWorld.createIngredientDto = createIngredientDto;
        world.ingredientWorld.ingredient = recipeBookClient.createIngredient(createIngredientDto);
    }

    @Then("I can retrieve the ingredient in the list")
    public void iCanRetrieveTheIngredientInTheList() {
        assertThat(recipeBookClient.getIngredients()).contains(world.ingredientWorld.ingredient);
    }

    @Then("I can retrieve the ingredient by its id")
    public void iCanRetrieveTheIngredientByItsId() {
        assertThat(recipeBookClient.getIngredient(world.ingredientWorld.ingredient.getId())).isEqualTo(world.ingredientWorld.ingredient);
    }

    @When("I update units")
    public void iUpdateUnits() {
        var allUnits = recipeBookClient.getAllUnits();
        var updateIngredientUnitsDto = new UpdateIngredientUnitsDto(Set.of(allUnits.get(0).getId(),
                allUnits.get(1).getId()));
        world.ingredientWorld.updatedUnits = updateIngredientUnitsDto;
        world.ingredientWorld.updatedIngredient = recipeBookClient.replaceIngredientUnits(world.ingredientWorld.ingredient.getId(),
                updateIngredientUnitsDto);
    }

    @Then("The ingredient should be updated with new units")
    public void theIngredientShouldBeUpdated() {
        assertThat(world.ingredientWorld.updatedIngredient)
                .returns(world.ingredientWorld.createIngredientDto.getName(), IngredientDto::getName);
        assertThat(world.ingredientWorld.updatedIngredient.getUnits())
                .extracting(UnitDto::getId)
                .containsExactlyInAnyOrderElementsOf(world.ingredientWorld.updatedUnits.getUnits());

    }

    @Then("The create unit response has all the data")
    public void theCreateUnitResponseHasAllTheData() {
        assertThat(world.ingredientWorld.unit)
                .returns(world.ingredientWorld.createUnitDto.getName(), UnitDto::getName);
    }

    @Then("The create ingredient response has all the data")
    public void theCreateIngredientResponseHasAllTheData() {
        assertThat(world.ingredientWorld.ingredient)
                .returns(world.ingredientWorld.createIngredientDto.getName(), IngredientDto::getName);
    }

    @When("I update types")
    public void iUpdateTypes() {
        updateType(Set.of(BEEF, CHICKEN));
    }

    public void iUpdateTypes(IngredientTypeEnum type) {
        updateType(Set.of(type));
    }

    private void updateType(Set<IngredientTypeEnum> type) {
        var updateIngredientUnitsDto = new UpdateIngredientTypesDto(type);
        world.ingredientWorld.updatedTypes = updateIngredientUnitsDto;
        world.ingredientWorld.updatedIngredient = recipeBookClient.replaceIngredientTypes(world.ingredientWorld.ingredient.getId(),
                updateIngredientUnitsDto);
    }

    @Then("The ingredient should be updated with new types")
    public void theIngredientShouldBeUpdatedWithNewTypes() {
        assertThat(world.ingredientWorld.updatedIngredient)
                .returns(world.ingredientWorld.createIngredientDto.getName(), IngredientDto::getName);

        assertThat(world.ingredientWorld.updatedIngredient.getTypes())
                .containsExactlyInAnyOrderElementsOf(world.ingredientWorld.updatedTypes.getTypes());
    }


}
