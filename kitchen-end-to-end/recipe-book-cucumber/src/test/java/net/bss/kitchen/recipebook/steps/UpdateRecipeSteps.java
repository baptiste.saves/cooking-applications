package net.bss.kitchen.recipebook.steps;

import io.cucumber.java.en.Given;
import net.bss.kitchen.recipebook.DtoFactory;
import net.bss.kitchen.recipebook.RecipeBookClient;
import net.bss.kitchen.recipebook.World;

public class UpdateRecipeSteps {
    private final World world;
    private final RecipeBookClient recipeBookClient;

    public UpdateRecipeSteps(World world, RecipeBookClient recipeBookClient) {
        this.world = world;
        this.recipeBookClient = recipeBookClient;
    }

    @Given("I update the recipe")
    public void iUpdateTheRecipe() {
        world.createRecipeWorld.createRecipeDto = DtoFactory.createRecipeDto(world.pictureId);
        recipeBookClient.updateRecipe(world.createdRecipesId.get(0), world.createRecipeWorld.createRecipeDto);
    }
}
