package net.bss.kitchen.recipebook;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.RandomUtils;
import org.openapitools.client.model.CreateUpdateMeasureDto;
import org.openapitools.client.model.CreateUpdateRecipeDto;
import org.openapitools.client.model.IngredientDto;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class DtoFactory {

    private static RecipeBookClient recipeBookClient;

    public DtoFactory(RecipeBookClient recipeBookClient) {
        DtoFactory.recipeBookClient = recipeBookClient;
    }

    public static CreateUpdateRecipeDto createRecipeDto(UUID pictureId) {
        var ingredients = recipeBookClient.getIngredients()
                .stream()
                .filter(ingredientDto -> CollectionUtils.isNotEmpty(ingredientDto.getUnits())).toList();

        return CreateUpdateRecipeDto
                .builder()
                .title("E2E Recipe")
                .yieldQuantity(RandomUtils.nextInt(1, 15))
                .pictureIds(List.of(pictureId))
                .link("https://www.marmiton.org/recettes/recette_tagliatelles-au-saumon-frais_11354.aspx")
                .content(
                        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.")
                .measures(List.of(
                        CreateUpdateMeasureDto.builder()
                                .quantity(0.001)
                                .ingredientId(ingredients.get(0).getId())
                                .unitId(ingredients.get(0).getUnits().stream().findFirst().orElseThrow().getId())
                                .build(),
                        CreateUpdateMeasureDto.builder()
                                .quantity(1000D)
                                .ingredientId(ingredients.get(1).getId())
                                .unitId(ingredients.get(1).getUnits().stream().findFirst().orElseThrow().getId())
                                .build(),
                        CreateUpdateMeasureDto.builder()
                                .quantity(55.576)
                                .ingredientId(ingredients.get(2).getId())
                                .unitId(ingredients.get(2).getUnits().stream().findFirst().orElseThrow().getId())
                                .build()
                ))
                .build();

    }

    public static CreateUpdateRecipeDto createRecipeDto(UUID pictureId, IngredientDto ingredient) {
        var recipeDto = createRecipeDto(pictureId);
        recipeDto.setMeasures(List.of(
                CreateUpdateMeasureDto.builder()
                        .quantity(0.001)
                        .ingredientId(ingredient.getId())
                        .unitId(ingredient.getUnits().stream().findFirst().orElseThrow().getId())
                        .build()
        ));
        return recipeDto;
    }
}
