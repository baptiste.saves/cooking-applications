package net.bss.kitchen.recipebook;

import org.openapitools.client.model.*;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static io.cucumber.spring.CucumberTestContext.SCOPE_CUCUMBER_GLUE;

@Component
@Scope(SCOPE_CUCUMBER_GLUE)
public class World {
    public CreateRecipeWorld createRecipeWorld = new CreateRecipeWorld();
    public IngredientWorld ingredientWorld = new IngredientWorld();
    public List<UUID> createdRecipesId = new ArrayList<>();
    public List<CreateUpdateRecipeDto> createdRecipes = new ArrayList<>();
    public List<RecipeListDto> recipes;
    public RecipeDto recipe;
    public UUID pictureId;

    public static class IngredientWorld {
        public UnitDto unit;
        public IngredientDto ingredient;
        public CreateUnitDto createUnitDto;
        public CreateIngredientDto createIngredientDto;
        public IngredientDto updatedIngredient;
        public UpdateIngredientUnitsDto updatedUnits;
        public UpdateIngredientTypesDto updatedTypes;
    }


    public static class CreateRecipeWorld {
        public CreateUpdateRecipeDto createRecipeDto;
    }
}
