package net.bss.kitchen.recipebook.steps;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import net.bss.kitchen.recipebook.RecipeBookClient;
import net.bss.kitchen.recipebook.World;

import java.nio.file.Files;
import java.nio.file.Path;

import static org.assertj.core.api.Assertions.assertThat;

@AllArgsConstructor
public class PictureSteps {
    private final World world;
    private final RecipeBookClient recipeBookClient;

    @SneakyThrows
    @When("I upload a picture")
    public void iUploadAPicture() {
        world.pictureId = recipeBookClient.uploadPicture(Path.of(ClassLoader.getSystemResource(
                "pictures/tagliatelles.webp").toURI()).toFile());
    }

    @Then("The answer is the id")
    public void theAnswerIsTheId() {
        assertThat(world.pictureId).isNotNull();
    }

    @SneakyThrows
    @When("I download the picture")
    public void iDownloadThePicture() {
        assertThat(recipeBookClient.downloadPicture(world.pictureId)).isEqualTo(
                Files.readAllBytes(Path.of(ClassLoader.getSystemResource(
                        "pictures/tagliatelles.webp").toURI()))
        );
    }

    @Then("The picture has the same data")
    public void thePictureHasTheSameData() {
    }
}
