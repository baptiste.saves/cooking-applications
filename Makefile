.ONESHELL:
MAKEFLAGS += -j5 #Parallelisation

OPENAPI_URL=http://localhost:10000/v3/api-docs/

copy-openapi:
	if curl --output /dev/null --silent --fail -r 0-0 "${OPENAPI_URL}"; then
		wget -N -O kitchen-end-to-end/recipe-java-backend-sdk/src/main/resources/java-openapi.json ${OPENAPI_URL}
		wget -N -O recipe-python-backend/java-openapi.json ${OPENAPI_URL}
	fi


generate-python-sdk: copy-openapi
	cd recipe-python-backend
	rm -r java-api-python-client
	docker run --rm \
	--user 1000:1000 \
	-v ${PWD}/recipe-python-backend:/local openapitools/openapi-generator-cli:v7.7.0 \
	generate \
	-i /local/java-openapi.json \
	-p packageName=java_api_python_client \
	-g python -o /local/java-api-python-client/

build-java:
	cd recipe-java-backend
	mvn clean install -DskipTests
	docker build -t registry.gitlab.com/baptiste.saves/cooking-applications/java-api .


build-python:
	cd recipe-python-backend
	poetry install --sync
	docker build -t registry.gitlab.com/baptiste.saves/cooking-applications/python-api .


build-front:
	cd front-angular
	npm install
	npm run build

build-docker-nginx: build-front
	cp -r front-angular/dist/front-angular/ nginx
	docker build -t registry.gitlab.com/baptiste.saves/cooking-applications/nginx nginx


build: build-java build-python build-docker-nginx

login-registry:
	echo "${TOKEN_GITLAB}" | docker login registry.gitlab.com -u baptiste --password-stdin


generate-python-client:
	docker run --rm -v ${PWD}:/local --net "host" --user "1000:1000" openapitools/openapi-generator-cli:latest-release generate -i http://127.0.0.1:10000/v3/api-docs/ -g python -o /local/recipe-python-backend/java-api-python-client/ --package-name=java_api_python_client


run:
	mkdir -p database/mysql
	mkdir -p database/postgres

	docker compose up -d

stop:
	docker compose down

