import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {RecipeListComponent} from './recipes/recipe-list/recipe-list.component';
import {RecipeAddEditComponent} from "./recipes/recipe-add-edit/recipe-add-edit.component";
import {RecipeDisplayComponent} from "./recipes/recipe-display/recipe-display.component";
import {IngredientManageComponent} from "./recipes/ingredient-manage/ingredient-manage.component";
import {PlanningListComponent} from "./plannings/planning-list/planning-list.component";
import {PlanningAddEditComponent} from "./plannings/planning-add-edit/planning-add-edit.component";

const routes: Routes = [
  {
    path: 'recipe', children: [
      {path: 'list', component: RecipeListComponent},
      {path: 'create', component: RecipeAddEditComponent},
      {path: 'update/:id', component: RecipeAddEditComponent},
      {path: ':id', component: RecipeDisplayComponent},
    ],
  }, {
    path: 'planning', children: [
      {path: 'list', component: PlanningListComponent},
      {path: 'create', component: PlanningAddEditComponent},
      {path: ':id', component: PlanningAddEditComponent},
    ],
  },
  {path: 'ingredient', component: IngredientManageComponent},
  {path: '**', redirectTo: "/recipe/list"}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
