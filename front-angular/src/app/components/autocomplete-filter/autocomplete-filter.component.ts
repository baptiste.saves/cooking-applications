import {Component, Input, OnChanges, SimpleChanges} from '@angular/core';
import {Observable, startWith} from "rxjs";
import {FormControl} from "@angular/forms";
import {map} from "rxjs/operators";

@Component({
  selector: 'app-autocomplete-filter',
  templateUrl: './autocomplete-filter.component.html',
  styleUrls: ['./autocomplete-filter.component.scss'],

})
export class AutocompleteFilterComponent implements OnChanges {

  @Input()
  itemList: Object[];
  @Input()
  attribute: string;
  @Input()
  label: string;
  @Input()
  itemControl: FormControl

  filteredItemList: Observable<Object[]>;

  constructor() {
  }

  displayItemFn(object: Object): string {
    return object ? object[this.attribute] : '';
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['itemList']) {
      this.itemList = changes['itemList'].currentValue
      if (this.itemControl) {
        this.itemControl.setValue("")
      }
    }
    if (changes['attribute']) {
      this.attribute = changes['attribute'].currentValue
    }
    if (changes['itemControl'] && changes['itemControl'].currentValue) {
      this.itemControl = changes['itemControl'].currentValue

      this.filteredItemList = this.itemControl.valueChanges.pipe(
        startWith(''),
        map((value: Object | String) => {
          if (typeof value === 'string') {
            return value ? this._filter(value as string) : this.itemList;
          }
          return this._filter(value[this.attribute] as string)
        }),
      );
    }
  }

  private _filter(value: string): Object[] {
    const filterValue = value.toLowerCase();
    return this.itemList.filter(option => option[this.attribute].toLowerCase().includes(filterValue));
  }
}
