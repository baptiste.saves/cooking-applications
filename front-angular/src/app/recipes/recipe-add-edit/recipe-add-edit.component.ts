import {Component, OnInit} from '@angular/core';
import {AbstractControl, FormArray, FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {RecipeControllerService} from "../../api/java-api-client/services/recipe-controller.service";
import {ActivatedRoute, Router} from "@angular/router";
import {MatSnackBar} from "@angular/material/snack-bar";
import {IngredientControllerService} from "../../api/java-api-client/services/ingredient-controller.service";
import {IngredientDto} from "../../api/java-api-client/models/ingredient-dto";
import {UnitDto} from "../../api/java-api-client/models/unit-dto";
import {PictureControllerService} from "../../api/java-api-client/services/picture-controller.service";
import {DomSanitizer} from "@angular/platform-browser";
import {HttpClient} from "@angular/common/http";
import {RecipeService} from "../../api/python-api-client/services/recipe.service";
import {firstValueFrom} from "rxjs";
import {CreateUpdateRecipeDto} from "../../api/java-api-client/models/create-update-recipe-dto";

@Component({
  selector: 'app-recipe-create',
  templateUrl: './recipe-add-edit.component.html',
  styleUrls: ['./recipe-add-edit.component.scss']
})
export class RecipeAddEditComponent implements OnInit {
  recipeForm = this.formBuilder.group({
    title: ['', Validators.required],
    content: ['', Validators.required],
    picture: [''],
    yieldQuantity: [1, Validators.required],
    link: [''],
    measures: this.formBuilder.array([])
  });

  existingIngredientList: Array<IngredientDto> = [];

  displaySiteData: boolean = false;
  recipePicture: any;
  isAddMode: boolean;
  private id: string;

  constructor(private formBuilder: FormBuilder,
              private recipeControllerService: RecipeControllerService,
              private ingredientControllerService: IngredientControllerService,
              private recipePythonService: RecipeService,
              private router: Router,
              private route: ActivatedRoute,
              private snackBar: MatSnackBar,
              private pictureControllerService: PictureControllerService,
              private sanitizer: DomSanitizer,
              private http: HttpClient) {
  }

  get measures() {
    return this.recipeForm.controls["measures"] as FormArray;
  }

  async ngOnInit() {
    this.existingIngredientList = await firstValueFrom(this.ingredientControllerService.getIngredients())
    this.id = this.route.snapshot.params['id'];
    this.isAddMode = !this.id;
    if (!this.isAddMode) {
      let recipe = await firstValueFrom(this.recipeControllerService.getRecipe({id: this.id}))
      this.recipeForm.patchValue(recipe)

      let pictureId = recipe.pictureIds.at(0);
      this.recipeForm.controls.picture.setValue(pictureId)
      this.displayPicture(pictureId);

      recipe.measures.forEach((measure, index) => {
        this.addIngredient()
        let abstractControl = this.measures.controls.at(index) as FormGroup;
        abstractControl.controls['quantity'].setValue(measure.quantity)
        let ingredientDto = this.existingIngredientList.find(value1 => value1.id === measure.ingredientId);

        let unitDto = ingredientDto.units.find(value => value.id == measure.unitId)

        setTimeout(function () {
          abstractControl.controls['ingredient'].setValue(ingredientDto)
          abstractControl.controls['unit'].setValue(unitDto)
        }, 100);
      })
    }
  }

  onSubmit(): void {
    let value = this.recipeForm.value;
    let dto: CreateUpdateRecipeDto = {
      content: value.content,
      title: value.title,
      yieldQuantity: value.yieldQuantity,
      pictureIds: [value.picture],
      link: value.link,
      measures: value.measures?.map(value1 => {
        return {
          quantity: value1["quantity"],
          ingredientId: value1["ingredient"].id,
          unitId: value1["unit"].id
        }
      })
    }
    if (this.isAddMode) {
      this.recipeControllerService.createRecipe({body: dto}).subscribe(value => {
        this.snackBar.open('Recette créée', 'ok', {duration: 3000});
        this.router.navigateByUrl('/recipe/' + value)
      })
    } else {
      this.recipeControllerService.updateRecipe({id: this.id, body: dto}).subscribe(value => {
        this.snackBar.open('Recette mise à jour', 'ok', {duration: 3000});
        this.router.navigateByUrl('/recipe/' + value)
      })
    }
  }

  addIngredient() {
    const ingredientForm = this.formBuilder.group({
      quantity: ['', Validators.required],
      ingredient: new FormControl<IngredientDto | null>(null, [Validators.required, Validators.min(6)]),
      unit: new FormControl<UnitDto | null>(null, Validators.required),
      data: [{value: '', disabled: true}]
    })

    ingredientForm.controls.ingredient.valueChanges.subscribe((value: any) => {
      if (value.units) {
        ingredientForm.controls.unit.setValue((value as IngredientDto).units[0])
      } else {
        ingredientForm.controls.unit.setValue(null)
      }
    })

    this.measures.push(ingredientForm)
  }

  deleteIngredient(i: number) {
    this.measures.removeAt(i);
  }

  scrapRecipe() {
    this.recipePythonService.scrapRecipeFromAnotherSiteRecipeScrappingGet({url: this.recipeForm.controls.link.value})
      .subscribe(scrapped => {
        this.recipeForm.controls.title.setValue(scrapped.title)
        this.recipeForm.controls.content.setValue(scrapped.instructions.join("\n\n"))
        this.recipeForm.controls.yieldQuantity.setValue(scrapped.yield_quantity)
        this.recipeForm.controls.measures.clear();

        if (scrapped.picture) {
          this.http.get(scrapped.picture, {responseType: 'blob'}).subscribe((resp: Blob) => {
            this.uploadAndGetPicture(resp);
          });
        }

        scrapped.ingredients.forEach((ingredientDto, index) => {
          this.addIngredient()
          let abstractControl = this.measures.controls.at(index) as FormGroup;
          abstractControl.controls['quantity'].setValue(ingredientDto.quantity ? ingredientDto.quantity : 1)
          abstractControl.controls['data'].setValue(ingredientDto.scraped_text)
          if (ingredientDto.id) {
            this.ingredientControllerService.getIngredient({id: ingredientDto.id}).subscribe(ingredient => {
              abstractControl.controls['ingredient'].setValue(ingredient)
              if (ingredientDto.unit) {
                abstractControl.controls['unit'].setValue(
                  ingredient.units.filter((unit, index1) => unit.id == ingredientDto.unit)[0]
                )
              }
            })
          }
          this.displaySiteData = true
        })
      })
  }

  handleFileInput(files: any) {
    this.uploadAndGetPicture((files as HTMLInputElement).files[0])
  }

  getUnitOptions(i: number): UnitDto[] {
    let value = (this.measures.controls.at(i) as FormGroup).controls['ingredient'].value;
    if (value) {
      return value.units
    }
    return []
  }

  getIngredientForm(measuresForm: AbstractControl<any>) {
    return (measuresForm as FormArray).controls['ingredient']
  }

  private displayPicture(pictureId) {
    this.pictureControllerService.getPicture({id: pictureId}).subscribe(blob => {
      // @ts-ignore
      const objectURL = URL.createObjectURL(blob);
      this.recipePicture = this.sanitizer.bypassSecurityTrustUrl(objectURL);
    })
  }

  private uploadAndGetPicture(resp: Blob) {
    this.pictureControllerService.uploadPicture(
      {
        body: {
          picture: resp
        }
      }
    ).subscribe(pictureId => {
      this.recipeForm.controls.picture.setValue(pictureId)
      this.displayPicture(pictureId)
    })
  }
}
