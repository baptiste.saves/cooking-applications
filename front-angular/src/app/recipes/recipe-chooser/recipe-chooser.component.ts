import {Component, ElementRef, EventEmitter, OnInit, Optional, Output, ViewChild} from '@angular/core';
import {
  IngredientControllerService,
  PictureControllerService,
  RecipeControllerService
} from '../../api/java-api-client/services';
import {RecipeListDto} from "../../api/java-api-client/models/recipe-list-dto";
import {DomSanitizer} from "@angular/platform-browser";
import {FormControl} from "@angular/forms";
import {COMMA, ENTER} from "@angular/cdk/keycodes";
import {MatAutocompleteSelectedEvent} from "@angular/material/autocomplete";
import {Observable, startWith} from "rxjs";
import {map} from "rxjs/operators";
import {IngredientTypeEnum} from "../../api/java-api-client/models/ingredient-type-enum";
import {ActivatedRoute, Router} from '@angular/router';
import {MatDialogRef} from "@angular/material/dialog";

@Component({
  selector: 'app-recipe-chooser',
  templateUrl: './recipe-chooser.component.html',
  styleUrls: ['./recipe-chooser.component.scss']
})
export class RecipeChooserComponent implements OnInit {

  @Output()
  onSelected = new EventEmitter<RecipeListDto>();

  separatorKeysCodes: number[] = [ENTER, COMMA];
  recipes: RecipeListDto[] = [];


  typeContainsForm = new FormControl('');
  @ViewChild('ingredientTypeContainsInput') ingredientTypeContainsInput: ElementRef<HTMLInputElement>;
  filteredIngredientTypeContains: Observable<IngredientTypeEnum[]>;
  selectedIngredientTypeContains: Array<IngredientTypeEnum> = [];


  typeDoesNotContainForm = new FormControl('');
  @ViewChild('ingredientTypeDoesNotContainInput') ingredientTypeDoesNotContainInput: ElementRef<HTMLInputElement>;
  filteredIngredientTypeDoesNotContain: Observable<IngredientTypeEnum[]>;
  selectedIngredientTypeDoesNotContain: Array<IngredientTypeEnum> = [];
  numberOfCols: number;
  private allIngredientTypes: Array<IngredientTypeEnum>;
  private allRecipe: RecipeListDto[];

  constructor(private recipeControllerService: RecipeControllerService,
              private ingredientControllerService: IngredientControllerService,
              private pictureControllerService: PictureControllerService,
              private sanitizer: DomSanitizer,
              private router: Router,
              private activatedRoute: ActivatedRoute,
              @Optional() public dialogRef: MatDialogRef<RecipeChooserComponent>) {
  }

  onResize(event) {
    this.updateNumberOfCols(event.target.innerWidth);
  }

  ngOnInit(): void {
    this.updateNumberOfCols(window.innerWidth);
    this.recipeControllerService.getRecipes().subscribe((value: RecipeListDto[]) => {
      value.filter(value1 => value1.picture).forEach(recipe => {
        this.pictureControllerService.getPicture({id: recipe.picture}).subscribe(blob => {
          // @ts-ignore
          const objectURL = URL.createObjectURL(blob);
          recipe['pictureBlob'] = this.sanitizer.bypassSecurityTrustUrl(objectURL);
        })
      })
      this.allRecipe = value
    })

    this.ingredientControllerService.getAllIngredientTypes().subscribe(typesDto => {
      this.allIngredientTypes = typesDto.types


      let paramContains = this.activatedRoute.snapshot.queryParamMap.getAll('typeContains');
      if (paramContains) {
        this.allIngredientTypes.filter(value => paramContains.includes(value))
          .forEach((ingredient: IngredientTypeEnum) => {
            this.allIngredientTypes.splice(this.allIngredientTypes.indexOf(ingredient), 1)
            this.selectedIngredientTypeContains.push(ingredient);
          })
      }
      let paramDoesNotContain = this.activatedRoute.snapshot.queryParamMap.getAll('typeDoesNotContain');
      if (paramDoesNotContain) {
        this.allIngredientTypes.filter(value => {
          return paramDoesNotContain.includes(value)
        }).forEach((ingredient: IngredientTypeEnum) => {
          this.allIngredientTypes.splice(this.allIngredientTypes.indexOf(ingredient), 1)
          this.selectedIngredientTypeDoesNotContain.push(ingredient);
        });
      }

      this.updateDisplayedRecipes()

      this.filteredIngredientTypeContains = this.typeContainsForm.valueChanges.pipe(
        startWith(null),
        map((ingredient: string | null) => (ingredient ? this._filter(ingredient) : this.allIngredientTypes.slice())),
      );

      this.filteredIngredientTypeDoesNotContain = this.typeDoesNotContainForm.valueChanges.pipe(
        startWith(null),
        map((ingredient: string | null) => (ingredient ? this._filter(ingredient) : this.allIngredientTypes.slice())),
      );
    })
  }

  removeContains(ingredient: IngredientTypeEnum): void {
    const index = this.selectedIngredientTypeContains.indexOf(ingredient);
    this.selectedIngredientTypeContains.splice(index, 1);
    this.allIngredientTypes.push(ingredient)
    this.refreshAutocomplete();
    this.updateDisplayedRecipes();
  }

  selectedContains(event: MatAutocompleteSelectedEvent): void {
    let ingredient = event.option.viewValue as IngredientTypeEnum;
    this.allIngredientTypes.splice(this.allIngredientTypes.indexOf(ingredient), 1)
    this.selectedIngredientTypeContains.push(ingredient);
    this.ingredientTypeContainsInput.nativeElement.value = '';
    this.typeContainsForm.setValue(null);
    this.refreshAutocomplete();
    this.updateDisplayedRecipes();
  }

  removeDoesNotContain(ingredient: IngredientTypeEnum) {
    const index = this.selectedIngredientTypeDoesNotContain.indexOf(ingredient);
    this.selectedIngredientTypeDoesNotContain.splice(index, 1);
    this.allIngredientTypes.push(ingredient)
    this.refreshAutocomplete();
    this.updateDisplayedRecipes();
  }

  selectedDoesNotContain(event: MatAutocompleteSelectedEvent) {
    let ingredient = event.option.viewValue as IngredientTypeEnum;
    this.addSelectedDoesNotContain(ingredient);
  }

  setVege() {
    this.setNoFilter();

    [IngredientTypeEnum.Beef,
      IngredientTypeEnum.Duck,
      IngredientTypeEnum.Chicken,
      IngredientTypeEnum.Pork,
      IngredientTypeEnum.Veal
    ].forEach((ingredient: IngredientTypeEnum) => {
      this.allIngredientTypes.splice(this.allIngredientTypes.indexOf(ingredient), 1)
      this.selectedIngredientTypeDoesNotContain.push(ingredient);
    })
    this.refreshAutocomplete();
    this.updateDisplayedRecipes();
  }

  setNoFilter() {
    this.allIngredientTypes.push(...this.selectedIngredientTypeContains)
    this.allIngredientTypes.push(...this.selectedIngredientTypeDoesNotContain)
    this.selectedIngredientTypeContains = []
    this.selectedIngredientTypeDoesNotContain = []
    this.refreshAutocomplete();
    this.updateDisplayedRecipes();
  }

  onClick(recipe: RecipeListDto) {
    this.onSelected.emit(recipe);
    if (this.dialogRef) {
      this.dialogRef.close(recipe)
    }
  }

  private updateNumberOfCols(width: number) {
    this.numberOfCols = Math.trunc(width / 280);
  }

  private addSelectedDoesNotContain(ingredient: IngredientTypeEnum) {
    this.allIngredientTypes.splice(this.allIngredientTypes.indexOf(ingredient), 1)
    this.selectedIngredientTypeDoesNotContain.push(ingredient);
    this.ingredientTypeDoesNotContainInput.nativeElement.value = '';
    this.typeDoesNotContainForm.setValue(null);
    this.refreshAutocomplete();
    this.updateDisplayedRecipes();
  }

  private refreshAutocomplete() {
    this.typeContainsForm.setValue(this.typeContainsForm.value);
    this.typeDoesNotContainForm.setValue(this.typeDoesNotContainForm.value);
  }

  private updateDisplayedRecipes() {
    this.router.navigate(
      [],
      {
        relativeTo: this.activatedRoute,
        queryParams: {
          typeContains: this.selectedIngredientTypeContains,
          typeDoesNotContain: this.selectedIngredientTypeDoesNotContain
        },
        queryParamsHandling: 'merge',
      });

    this.recipeControllerService.getRecipes({
      typeContains: this.selectedIngredientTypeContains,
      typeDoesNotContain: this.selectedIngredientTypeDoesNotContain
    }).subscribe((value: RecipeListDto[]) => {
      this.recipes = this.allRecipe.filter(recipe => {
          return value.map(value1 => value1.id).indexOf(recipe.id) != -1
        }
      )
    })
  }

  private _filter(value: string): IngredientTypeEnum[] {
    const filterValue = value.toLowerCase();

    return this.allIngredientTypes.filter(fruit => fruit.toLowerCase().includes(filterValue));
  }
}
