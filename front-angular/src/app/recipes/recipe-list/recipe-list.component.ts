import {Component, OnInit} from '@angular/core';
import {
  IngredientControllerService,
  PictureControllerService,
  RecipeControllerService
} from '../../api/java-api-client/services';
import {DomSanitizer} from "@angular/platform-browser";
import {ActivatedRoute, Router} from '@angular/router';
import {RecipeListDto} from "../../api/java-api-client/models/recipe-list-dto";

@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.scss']
})
export class RecipeListComponent implements OnInit {
  ngOnInit(): void {
  }

  constructor(private recipeControllerService: RecipeControllerService,
              private ingredientControllerService: IngredientControllerService,
              private pictureControllerService: PictureControllerService,
              private sanitizer: DomSanitizer,
              private router: Router,
              private activatedRoute: ActivatedRoute) {
  }

  onRecipeSelected(recipe: RecipeListDto) {
    this.router.navigateByUrl("/recipe/" + recipe.id)
  }
}
