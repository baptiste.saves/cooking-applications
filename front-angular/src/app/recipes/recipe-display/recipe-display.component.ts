import {Component, OnInit} from '@angular/core';
import {RecipeDto} from "../../api/java-api-client/models/recipe-dto";
import {RecipeControllerService} from "../../api/java-api-client/services/recipe-controller.service";
import {ActivatedRoute} from "@angular/router";
import {FormControl} from "@angular/forms";
import {PictureControllerService} from "../../api/java-api-client/services/picture-controller.service";
import {DomSanitizer} from "@angular/platform-browser";

@Component({
  selector: 'app-recipe-display',
  templateUrl: './recipe-display.component.html',
  styleUrls: ['./recipe-display.component.scss']
})
export class RecipeDisplayComponent implements OnInit {

  recipe: RecipeDto;
  yieldQuantity = new FormControl<number>(1);
  private oldValue: number;
  recipePicture: any;

  constructor(private recipeControllerService: RecipeControllerService,
              private pictureControllerService: PictureControllerService,
              private sanitizer: DomSanitizer,
              private route: ActivatedRoute
  ) {
  }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.recipeControllerService.getRecipe({id: params['id']}).subscribe(recipe => {
        this.recipe = recipe
        this.yieldQuantity.setValue(this.recipe.yieldQuantity)
        this.oldValue = this.recipe.yieldQuantity
        this.addYieldQuantityChangeListener();
        if (recipe.pictureIds) {
          this.pictureControllerService.getPicture({id: this.recipe.pictureIds[0]}).subscribe(blob => {
            // @ts-ignore
            const objectURL = URL.createObjectURL(blob);
            this.recipePicture = this.sanitizer.bypassSecurityTrustUrl(objectURL);
          })
        }
      })
    });

  }

  private addYieldQuantityChangeListener() {
    this.yieldQuantity.valueChanges.subscribe(value => {
      if (value > 0) {
        let ratio = value / this.oldValue
        this.oldValue = value
        this.recipe.measures.forEach(measure => {
          measure.quantity = measure.quantity * ratio
        })
      }
    })
  }
}
