import {Component, OnInit} from '@angular/core';
import {IngredientDto} from "../../api/java-api-client/models/ingredient-dto";
import {IngredientControllerService} from "../../api/java-api-client/services/ingredient-controller.service";
import {FormControl} from "@angular/forms";
import {UnitDto} from "../../api/java-api-client/models/unit-dto";
import {MatSnackBar} from "@angular/material/snack-bar";
import {IngredientTypeEnum} from "../../api/java-api-client/models/ingredient-type-enum";

@Component({
  selector: 'app-ingredient-manage',
  templateUrl: './ingredient-manage.component.html',
  styleUrls: ['./ingredient-manage.component.scss']
})
export class IngredientManageComponent implements OnInit {

  ingredientControl = new FormControl<IngredientDto>(null);
  unitControl = new FormControl('');
  allUnits: Array<UnitDto>;
  selectedUnits: Array<UnitDto> = [];
  selectedTypes: Array<IngredientTypeEnum>;
  allTypes: Array<IngredientTypeEnum>;
  allIngredients: Array<IngredientDto>;

  constructor(private ingredientControllerService: IngredientControllerService,
              private snackBar: MatSnackBar) {

  }

  ngOnInit(): void {
    this.ingredientControllerService.getAllIngredientTypes().subscribe(typeDto => {
      this.allTypes = typeDto.types.sort()
    })

    this.ingredientControllerService.getAllUnits().subscribe(units => {
      this.allUnits = units.sort((a, b) => a.name.localeCompare(b.name))
    })

    this.ingredientControllerService.getIngredients().subscribe(ingredients => {
      this.allIngredients = ingredients.sort((a, b) => a.name.localeCompare(b.name))


      this.ingredientControl.valueChanges.subscribe((value: IngredientDto) => {
        if (typeof value === 'string') {
          return
        }
        if (value) {
          this.selectedUnits = this.allUnits.filter(unit => {
            return value.units.map(value1 => value1.id).includes(unit.id)
          });
          this.selectedTypes = this.allTypes.filter(unit => {
            return value.types.map(value1 => value1).includes(unit)
          });
        }
      })
    })
  }

  displayIngredientFn(ingredientDto: IngredientDto): string {
    return ingredientDto ? ingredientDto.name : '';
  }

  onUnitSelectedChange($event: Array<UnitDto>) {
    let ingredient = this.ingredientControl.value;
    ingredient.units = $event
    this.ingredientControllerService.replaceIngredientUnits({
      id: ingredient.id,
      body: {
        units: $event.map(unit => unit.id)
      }
    }).subscribe()
  }

  onTypeSelectedChange($event: Array<IngredientTypeEnum>) {
    let ingredient = this.ingredientControl.value;
    ingredient.types = $event
    this.ingredientControllerService.replaceIngredientTypes({
      id: ingredient.id,
      body: {
        types: $event
      }
    }).subscribe()
  }

  addIngredient() {
    let ingredient = this.ingredientControl.value;
    if (typeof ingredient === 'string') {
      this.ingredientControllerService.createIngredient({body: {name: ingredient}})
        .subscribe(value => {
          this.allIngredients.push(value)
          this.allIngredients.sort((a, b) => a.name.localeCompare(b.name))
          this.ingredientControl.setValue(value)
          this.selectedUnits = this.allUnits.filter(value1 => value1.name === 'Unit')
          this.onUnitSelectedChange(this.selectedUnits)
          this.snackBar.open('Ingrédient créé', 'ok', {duration: 3000});
        })
    }
  }

  addUnit() {
    let unit = this.unitControl.value;
    this.ingredientControllerService.createUnit({body: {name: unit}})
      .subscribe(value => {
        this.allUnits.push(value)
        this.allUnits.sort((a, b) => a.name.localeCompare(b.name))
      })
  }

  private _filter(value: string): IngredientDto[] {
    const filterValue = value.toLowerCase();
    return this.allIngredients.filter(option => option.name.toLowerCase().includes(filterValue));
  }
}
