/* tslint:disable */
/* eslint-disable */
export interface CreateUpdateMeasureDto {
  ingredientId: string;
  quantity: number;
  unitId: string;
}
