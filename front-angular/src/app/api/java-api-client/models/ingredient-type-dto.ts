/* tslint:disable */
/* eslint-disable */
import {IngredientTypeEnum} from './ingredient-type-enum';

export interface IngredientTypeDto {
  types?: Array<IngredientTypeEnum>;
}
