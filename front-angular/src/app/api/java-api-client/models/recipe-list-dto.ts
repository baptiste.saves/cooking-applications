/* tslint:disable */
/* eslint-disable */
export interface RecipeListDto {
  createdAt?: string;
  id?: string;
  picture?: string;
  title?: string;
}
