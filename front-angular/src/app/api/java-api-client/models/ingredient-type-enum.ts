/* tslint:disable */
/* eslint-disable */
export enum IngredientTypeEnum {
  Vegetable = 'VEGETABLE',
  Fruit = 'FRUIT',
  Beef = 'BEEF',
  Duck = 'DUCK',
  Chicken = 'CHICKEN',
  Fish = 'FISH',
  Alcohol = 'ALCOHOL',
  Pasta = 'PASTA',
  Rice = 'RICE',
  Pork = 'PORK',
  Dairy = 'DAIRY',
  Veal = 'VEAL'
}
