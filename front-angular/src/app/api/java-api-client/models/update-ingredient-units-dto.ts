/* tslint:disable */
/* eslint-disable */
export interface UpdateIngredientUnitsDto {
  units: Array<string>;
}
