/* tslint:disable */
/* eslint-disable */
import {IngredientTypeEnum} from './ingredient-type-enum';
import {UnitDto} from './unit-dto';

export interface IngredientDto {
  id: string;
  name: string;
  types: Array<IngredientTypeEnum>;
  units: Array<UnitDto>;
}
