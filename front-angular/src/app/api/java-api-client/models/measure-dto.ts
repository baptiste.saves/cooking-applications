/* tslint:disable */
/* eslint-disable */
export interface MeasureDto {
  ingredient: string;
  ingredientId: string;
  quantity: number;
  unit: string;
  unitId: string;
}
