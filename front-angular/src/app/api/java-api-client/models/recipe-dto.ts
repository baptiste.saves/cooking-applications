/* tslint:disable */
/* eslint-disable */
import {MeasureDto} from './measure-dto';

export interface RecipeDto {
  content: string;
  createdAt: string;
  id: string;
  link?: string;
  measures: Array<MeasureDto>;
  pictureIds?: Array<string>;
  title: string;
  yieldQuantity: number;
}
