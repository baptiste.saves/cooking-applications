/* tslint:disable */
/* eslint-disable */
import {CreateUpdateMeasureDto} from './create-update-measure-dto';

export interface CreateUpdateRecipeDto {
  content: string;
  link?: string;
  measures: Array<CreateUpdateMeasureDto>;
  pictureIds?: Array<string>;
  title: string;
  yieldQuantity: number;
}
