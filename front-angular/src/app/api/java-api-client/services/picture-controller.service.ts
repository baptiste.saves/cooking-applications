/* tslint:disable */
/* eslint-disable */
import {Injectable} from '@angular/core';
import {HttpClient, HttpContext, HttpResponse} from '@angular/common/http';
import {BaseService} from '../base-service';
import {ApiConfiguration} from '../api-configuration';
import {StrictHttpResponse} from '../strict-http-response';
import {RequestBuilder} from '../request-builder';
import {Observable} from 'rxjs';
import {filter, map} from 'rxjs/operators';


@Injectable({
  providedIn: 'root',
})
export class PictureControllerService extends BaseService {
  constructor(
    config: ApiConfiguration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Path part for operation uploadPicture
   */
  static readonly UploadPicturePath = '/picture';
  /**
   * Path part for operation getPicture
   */
  static readonly GetPicturePath = '/picture/{id}';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `uploadPicture()` instead.
   *
   * This method sends `multipart/form-data` and handles request body of type `multipart/form-data`.
   */
  uploadPicture$Response(params?: {
                           body?: {
                             'picture': Blob;
                           }
                         },
                         context?: HttpContext
  ): Observable<StrictHttpResponse<string>> {

    const rb = new RequestBuilder(this.rootUrl, PictureControllerService.UploadPicturePath, 'post');
    if (params) {
      rb.body(params.body, 'multipart/form-data');
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<string>;
      })
    );
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `uploadPicture$Response()` instead.
   *
   * This method sends `multipart/form-data` and handles request body of type `multipart/form-data`.
   */
  uploadPicture(params?: {
                  body?: {
                    'picture': Blob;
                  }
                },
                context?: HttpContext
  ): Observable<string> {

    return this.uploadPicture$Response(params, context).pipe(
      map((r: StrictHttpResponse<string>) => r.body as string)
    );
  }

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `getPicture()` instead.
   *
   * This method doesn't expect any request body.
   */
  getPicture$Response(params: {
                        id: string;
                      },
                      context?: HttpContext
  ): Observable<StrictHttpResponse<string>> {

    const rb = new RequestBuilder(this.rootUrl, PictureControllerService.GetPicturePath, 'get');
    if (params) {
      rb.path('id', params.id, {});
    }

    return this.http.request(rb.build({
      responseType: 'blob',
      accept: 'application/octet-stream',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<string>;
      })
    );
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `getPicture$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  getPicture(params: {
               id: string;
             },
             context?: HttpContext
  ): Observable<string> {

    return this.getPicture$Response(params, context).pipe(
      map((r: StrictHttpResponse<string>) => r.body as string)
    );
  }

}
