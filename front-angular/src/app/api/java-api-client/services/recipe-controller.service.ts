/* tslint:disable */
/* eslint-disable */
import {Injectable} from '@angular/core';
import {HttpClient, HttpContext, HttpResponse} from '@angular/common/http';
import {BaseService} from '../base-service';
import {ApiConfiguration} from '../api-configuration';
import {StrictHttpResponse} from '../strict-http-response';
import {RequestBuilder} from '../request-builder';
import {Observable} from 'rxjs';
import {filter, map} from 'rxjs/operators';

import {CreateUpdateRecipeDto} from '../models/create-update-recipe-dto';
import {IngredientTypeEnum} from '../models/ingredient-type-enum';
import {RecipeDto} from '../models/recipe-dto';
import {RecipeListDto} from '../models/recipe-list-dto';

@Injectable({
  providedIn: 'root',
})
export class RecipeControllerService extends BaseService {
  constructor(
    config: ApiConfiguration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Path part for operation getRecipe
   */
  static readonly GetRecipePath = '/recipe/{id}';
  /**
   * Path part for operation updateRecipe
   */
  static readonly UpdateRecipePath = '/recipe/{id}';
  /**
   * Path part for operation getRecipes
   */
  static readonly GetRecipesPath = '/recipe';
  /**
   * Path part for operation createRecipe
   */
  static readonly CreateRecipePath = '/recipe';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `getRecipe()` instead.
   *
   * This method doesn't expect any request body.
   */
  getRecipe$Response(params: {
                       id: string;
                     },
                     context?: HttpContext
  ): Observable<StrictHttpResponse<RecipeDto>> {

    const rb = new RequestBuilder(this.rootUrl, RecipeControllerService.GetRecipePath, 'get');
    if (params) {
      rb.path('id', params.id, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<RecipeDto>;
      })
    );
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `getRecipe$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  getRecipe(params: {
              id: string;
            },
            context?: HttpContext
  ): Observable<RecipeDto> {

    return this.getRecipe$Response(params, context).pipe(
      map((r: StrictHttpResponse<RecipeDto>) => r.body as RecipeDto)
    );
  }

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `updateRecipe()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  updateRecipe$Response(params: {
                          id: string;
                          body: CreateUpdateRecipeDto
                        },
                        context?: HttpContext
  ): Observable<StrictHttpResponse<string>> {

    const rb = new RequestBuilder(this.rootUrl, RecipeControllerService.UpdateRecipePath, 'put');
    if (params) {
      rb.path('id', params.id, {});
      rb.body(params.body, 'application/json');
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<string>;
      })
    );
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `updateRecipe$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  updateRecipe(params: {
                 id: string;
                 body: CreateUpdateRecipeDto
               },
               context?: HttpContext
  ): Observable<string> {

    return this.updateRecipe$Response(params, context).pipe(
      map((r: StrictHttpResponse<string>) => r.body as string)
    );
  }

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `getRecipes()` instead.
   *
   * This method doesn't expect any request body.
   */
  getRecipes$Response(params?: {
                        typeContains?: Array<IngredientTypeEnum>;
                        typeDoesNotContain?: Array<IngredientTypeEnum>;
                      },
                      context?: HttpContext
  ): Observable<StrictHttpResponse<Array<RecipeListDto>>> {

    const rb = new RequestBuilder(this.rootUrl, RecipeControllerService.GetRecipesPath, 'get');
    if (params) {
      rb.query('typeContains', params.typeContains, {});
      rb.query('typeDoesNotContain', params.typeDoesNotContain, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<Array<RecipeListDto>>;
      })
    );
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `getRecipes$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  getRecipes(params?: {
               typeContains?: Array<IngredientTypeEnum>;
               typeDoesNotContain?: Array<IngredientTypeEnum>;
             },
             context?: HttpContext
  ): Observable<Array<RecipeListDto>> {

    return this.getRecipes$Response(params, context).pipe(
      map((r: StrictHttpResponse<Array<RecipeListDto>>) => r.body as Array<RecipeListDto>)
    );
  }

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `createRecipe()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  createRecipe$Response(params: {
                          body: CreateUpdateRecipeDto
                        },
                        context?: HttpContext
  ): Observable<StrictHttpResponse<string>> {

    const rb = new RequestBuilder(this.rootUrl, RecipeControllerService.CreateRecipePath, 'post');
    if (params) {
      rb.body(params.body, 'application/json');
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<string>;
      })
    );
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `createRecipe$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  createRecipe(params: {
                 body: CreateUpdateRecipeDto
               },
               context?: HttpContext
  ): Observable<string> {

    return this.createRecipe$Response(params, context).pipe(
      map((r: StrictHttpResponse<string>) => r.body as string)
    );
  }

}
