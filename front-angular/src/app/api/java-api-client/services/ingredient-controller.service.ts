/* tslint:disable */
/* eslint-disable */
import {Injectable} from '@angular/core';
import {HttpClient, HttpContext, HttpResponse} from '@angular/common/http';
import {BaseService} from '../base-service';
import {ApiConfiguration} from '../api-configuration';
import {StrictHttpResponse} from '../strict-http-response';
import {RequestBuilder} from '../request-builder';
import {Observable} from 'rxjs';
import {filter, map} from 'rxjs/operators';

import {CreateIngredientDto} from '../models/create-ingredient-dto';
import {CreateUnitDto} from '../models/create-unit-dto';
import {IngredientDto} from '../models/ingredient-dto';
import {IngredientTypeDto} from '../models/ingredient-type-dto';
import {UnitDto} from '../models/unit-dto';
import {UpdateIngredientTypesDto} from '../models/update-ingredient-types-dto';
import {UpdateIngredientUnitsDto} from '../models/update-ingredient-units-dto';

@Injectable({
  providedIn: 'root',
})
export class IngredientControllerService extends BaseService {
  constructor(
    config: ApiConfiguration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Path part for operation replaceIngredientUnits
   */
  static readonly ReplaceIngredientUnitsPath = '/ingredient/{id}/units';
  /**
   * Path part for operation replaceIngredientTypes
   */
  static readonly ReplaceIngredientTypesPath = '/ingredient/{id}/type';
  /**
   * Path part for operation getIngredients
   */
  static readonly GetIngredientsPath = '/ingredient';
  /**
   * Path part for operation createIngredient
   */
  static readonly CreateIngredientPath = '/ingredient';
  /**
   * Path part for operation getAllUnits
   */
  static readonly GetAllUnitsPath = '/ingredient/unit';
  /**
   * Path part for operation createUnit
   */
  static readonly CreateUnitPath = '/ingredient/unit';
  /**
   * Path part for operation getIngredient
   */
  static readonly GetIngredientPath = '/ingredient/{id}';
  /**
   * Path part for operation getAllIngredientTypes
   */
  static readonly GetAllIngredientTypesPath = '/ingredient/type';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `replaceIngredientUnits()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  replaceIngredientUnits$Response(params: {
                                    id: string;
                                    body: UpdateIngredientUnitsDto
                                  },
                                  context?: HttpContext
  ): Observable<StrictHttpResponse<IngredientDto>> {

    const rb = new RequestBuilder(this.rootUrl, IngredientControllerService.ReplaceIngredientUnitsPath, 'put');
    if (params) {
      rb.path('id', params.id, {});
      rb.body(params.body, 'application/json');
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<IngredientDto>;
      })
    );
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `replaceIngredientUnits$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  replaceIngredientUnits(params: {
                           id: string;
                           body: UpdateIngredientUnitsDto
                         },
                         context?: HttpContext
  ): Observable<IngredientDto> {

    return this.replaceIngredientUnits$Response(params, context).pipe(
      map((r: StrictHttpResponse<IngredientDto>) => r.body as IngredientDto)
    );
  }

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `replaceIngredientTypes()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  replaceIngredientTypes$Response(params: {
                                    id: string;
                                    body: UpdateIngredientTypesDto
                                  },
                                  context?: HttpContext
  ): Observable<StrictHttpResponse<IngredientDto>> {

    const rb = new RequestBuilder(this.rootUrl, IngredientControllerService.ReplaceIngredientTypesPath, 'put');
    if (params) {
      rb.path('id', params.id, {});
      rb.body(params.body, 'application/json');
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<IngredientDto>;
      })
    );
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `replaceIngredientTypes$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  replaceIngredientTypes(params: {
                           id: string;
                           body: UpdateIngredientTypesDto
                         },
                         context?: HttpContext
  ): Observable<IngredientDto> {

    return this.replaceIngredientTypes$Response(params, context).pipe(
      map((r: StrictHttpResponse<IngredientDto>) => r.body as IngredientDto)
    );
  }

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `getIngredients()` instead.
   *
   * This method doesn't expect any request body.
   */
  getIngredients$Response(params?: {
                            name?: string;
                          },
                          context?: HttpContext
  ): Observable<StrictHttpResponse<Array<IngredientDto>>> {

    const rb = new RequestBuilder(this.rootUrl, IngredientControllerService.GetIngredientsPath, 'get');
    if (params) {
      rb.query('name', params.name, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<Array<IngredientDto>>;
      })
    );
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `getIngredients$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  getIngredients(params?: {
                   name?: string;
                 },
                 context?: HttpContext
  ): Observable<Array<IngredientDto>> {

    return this.getIngredients$Response(params, context).pipe(
      map((r: StrictHttpResponse<Array<IngredientDto>>) => r.body as Array<IngredientDto>)
    );
  }

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `createIngredient()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  createIngredient$Response(params: {
                              body: CreateIngredientDto
                            },
                            context?: HttpContext
  ): Observable<StrictHttpResponse<IngredientDto>> {

    const rb = new RequestBuilder(this.rootUrl, IngredientControllerService.CreateIngredientPath, 'post');
    if (params) {
      rb.body(params.body, 'application/json');
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<IngredientDto>;
      })
    );
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `createIngredient$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  createIngredient(params: {
                     body: CreateIngredientDto
                   },
                   context?: HttpContext
  ): Observable<IngredientDto> {

    return this.createIngredient$Response(params, context).pipe(
      map((r: StrictHttpResponse<IngredientDto>) => r.body as IngredientDto)
    );
  }

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `getAllUnits()` instead.
   *
   * This method doesn't expect any request body.
   */
  getAllUnits$Response(params?: {},
                       context?: HttpContext
  ): Observable<StrictHttpResponse<Array<UnitDto>>> {

    const rb = new RequestBuilder(this.rootUrl, IngredientControllerService.GetAllUnitsPath, 'get');
    if (params) {
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<Array<UnitDto>>;
      })
    );
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `getAllUnits$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  getAllUnits(params?: {},
              context?: HttpContext
  ): Observable<Array<UnitDto>> {

    return this.getAllUnits$Response(params, context).pipe(
      map((r: StrictHttpResponse<Array<UnitDto>>) => r.body as Array<UnitDto>)
    );
  }

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `createUnit()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  createUnit$Response(params: {
                        body: CreateUnitDto
                      },
                      context?: HttpContext
  ): Observable<StrictHttpResponse<UnitDto>> {

    const rb = new RequestBuilder(this.rootUrl, IngredientControllerService.CreateUnitPath, 'post');
    if (params) {
      rb.body(params.body, 'application/json');
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<UnitDto>;
      })
    );
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `createUnit$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  createUnit(params: {
               body: CreateUnitDto
             },
             context?: HttpContext
  ): Observable<UnitDto> {

    return this.createUnit$Response(params, context).pipe(
      map((r: StrictHttpResponse<UnitDto>) => r.body as UnitDto)
    );
  }

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `getIngredient()` instead.
   *
   * This method doesn't expect any request body.
   */
  getIngredient$Response(params: {
                           id: string;
                         },
                         context?: HttpContext
  ): Observable<StrictHttpResponse<IngredientDto>> {

    const rb = new RequestBuilder(this.rootUrl, IngredientControllerService.GetIngredientPath, 'get');
    if (params) {
      rb.path('id', params.id, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<IngredientDto>;
      })
    );
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `getIngredient$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  getIngredient(params: {
                  id: string;
                },
                context?: HttpContext
  ): Observable<IngredientDto> {

    return this.getIngredient$Response(params, context).pipe(
      map((r: StrictHttpResponse<IngredientDto>) => r.body as IngredientDto)
    );
  }

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `getAllIngredientTypes()` instead.
   *
   * This method doesn't expect any request body.
   */
  getAllIngredientTypes$Response(params?: {},
                                 context?: HttpContext
  ): Observable<StrictHttpResponse<IngredientTypeDto>> {

    const rb = new RequestBuilder(this.rootUrl, IngredientControllerService.GetAllIngredientTypesPath, 'get');
    if (params) {
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<IngredientTypeDto>;
      })
    );
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `getAllIngredientTypes$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  getAllIngredientTypes(params?: {},
                        context?: HttpContext
  ): Observable<IngredientTypeDto> {

    return this.getAllIngredientTypes$Response(params, context).pipe(
      map((r: StrictHttpResponse<IngredientTypeDto>) => r.body as IngredientTypeDto)
    );
  }

}
