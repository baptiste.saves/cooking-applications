/* tslint:disable */
/* eslint-disable */
import {ModuleWithProviders, NgModule, Optional, SkipSelf} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ApiConfiguration, ApiConfigurationParams} from './api-configuration';

import {RecipeControllerService} from './services/recipe-controller.service';
import {IngredientControllerService} from './services/ingredient-controller.service';
import {PictureControllerService} from './services/picture-controller.service';

/**
 * Module that provides all services and configuration.
 */
@NgModule({
  imports: [],
  exports: [],
  declarations: [],
  providers: [
    RecipeControllerService,
    IngredientControllerService,
    PictureControllerService,
    ApiConfiguration
  ],
})
export class ApiJavaModule {
  static forRoot(params: ApiConfigurationParams): ModuleWithProviders<ApiJavaModule> {
    return {
      ngModule: ApiJavaModule,
      providers: [
        {
          provide: ApiConfiguration,
          useValue: params
        }
      ]
    }
  }

  constructor(
    @Optional() @SkipSelf() parentModule: ApiJavaModule,
    @Optional() http: HttpClient
  ) {
    if (parentModule) {
      throw new Error('ApiJavaModule is already loaded. Import in your base AppModule only.');
    }
    if (!http) {
      throw new Error('You need to import the HttpClientModule in your AppModule! \n' +
        'See also https://github.com/angular/angular/issues/20575');
    }
  }
}
