/* tslint:disable */
/* eslint-disable */
import {MenuDto} from './menu-dto';

export interface CreateUpdatePlanning {
  menus: Array<MenuDto>;
  title: string;
}
