/* tslint:disable */
/* eslint-disable */
import {MenuDto} from './menu-dto';

export interface PlanningDto {
  created_date: string;
  id: number;
  menus: Array<MenuDto>;
  title: string;
}
