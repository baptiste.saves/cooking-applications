/* tslint:disable */
/* eslint-disable */
export interface PossibleIngredientDto {
  id?: string;
  quantity?: number;
  scraped_text: string;
  unit?: string;
}
