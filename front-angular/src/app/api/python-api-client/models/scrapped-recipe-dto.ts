/* tslint:disable */
/* eslint-disable */
import {PossibleIngredientDto} from './possible-ingredient-dto';

export interface ScrappedRecipeDto {
  ingredients: Array<PossibleIngredientDto>;
  instructions: Array<string>;
  picture: string;
  title: string;
  url: string;
  yield_quantity: number;
}
