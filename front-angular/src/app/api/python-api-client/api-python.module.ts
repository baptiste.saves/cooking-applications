/* tslint:disable */
/* eslint-disable */
import {ModuleWithProviders, NgModule, Optional, SkipSelf} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ApiConfiguration, ApiConfigurationParams} from './api-configuration';

import {RecipeService} from './services/recipe.service';
import {PlanningService} from './services/planning.service';
import {ApiService} from './services/api.service';

/**
 * Module that provides all services and configuration.
 */
@NgModule({
  imports: [],
  exports: [],
  declarations: [],
  providers: [
    RecipeService,
    PlanningService,
    ApiService,
    ApiConfiguration
  ],
})
export class ApiPythonModule {
  static forRoot(params: ApiConfigurationParams): ModuleWithProviders<ApiPythonModule> {
    return {
      ngModule: ApiPythonModule,
      providers: [
        {
          provide: ApiConfiguration,
          useValue: params
        }
      ]
    }
  }

  constructor(
    @Optional() @SkipSelf() parentModule: ApiPythonModule,
    @Optional() http: HttpClient
  ) {
    if (parentModule) {
      throw new Error('ApiPythonModule is already loaded. Import in your base AppModule only.');
    }
    if (!http) {
      throw new Error('You need to import the HttpClientModule in your AppModule! \n' +
        'See also https://github.com/angular/angular/issues/20575');
    }
  }
}
