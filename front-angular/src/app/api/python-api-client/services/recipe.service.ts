/* tslint:disable */
/* eslint-disable */
import {Injectable} from '@angular/core';
import {HttpClient, HttpContext, HttpResponse} from '@angular/common/http';
import {BaseService} from '../base-service';
import {ApiConfiguration} from '../api-configuration';
import {StrictHttpResponse} from '../strict-http-response';
import {RequestBuilder} from '../request-builder';
import {Observable} from 'rxjs';
import {filter, map} from 'rxjs/operators';

import {ScrappedRecipeDto} from '../models/scrapped-recipe-dto';

@Injectable({
  providedIn: 'root',
})
export class RecipeService extends BaseService {
  constructor(
    config: ApiConfiguration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Path part for operation scrapRecipeFromAnotherSiteRecipeScrappingGet
   */
  static readonly ScrapRecipeFromAnotherSiteRecipeScrappingGetPath = '/recipe/scrapping/';

  /**
   * Scrap Recipe From Another Site.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scrapRecipeFromAnotherSiteRecipeScrappingGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  scrapRecipeFromAnotherSiteRecipeScrappingGet$Response(params: {
                                                          url: string;
                                                        },
                                                        context?: HttpContext
  ): Observable<StrictHttpResponse<ScrappedRecipeDto>> {

    const rb = new RequestBuilder(this.rootUrl, RecipeService.ScrapRecipeFromAnotherSiteRecipeScrappingGetPath, 'get');
    if (params) {
      rb.query('url', params.url, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<ScrappedRecipeDto>;
      })
    );
  }

  /**
   * Scrap Recipe From Another Site.
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `scrapRecipeFromAnotherSiteRecipeScrappingGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  scrapRecipeFromAnotherSiteRecipeScrappingGet(params: {
                                                 url: string;
                                               },
                                               context?: HttpContext
  ): Observable<ScrappedRecipeDto> {

    return this.scrapRecipeFromAnotherSiteRecipeScrappingGet$Response(params, context).pipe(
      map((r: StrictHttpResponse<ScrappedRecipeDto>) => r.body as ScrappedRecipeDto)
    );
  }

}
