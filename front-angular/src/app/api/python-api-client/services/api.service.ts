/* tslint:disable */
/* eslint-disable */
import {Injectable} from '@angular/core';
import {HttpClient, HttpContext, HttpResponse} from '@angular/common/http';
import {BaseService} from '../base-service';
import {ApiConfiguration} from '../api-configuration';
import {StrictHttpResponse} from '../strict-http-response';
import {RequestBuilder} from '../request-builder';
import {Observable} from 'rxjs';
import {filter, map} from 'rxjs/operators';


@Injectable({
  providedIn: 'root',
})
export class ApiService extends BaseService {
  /**
   * Path part for operation readRootGet
   */
  static readonly ReadRootGetPath = '/';

  constructor(
      config: ApiConfiguration,
      http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Read Root.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `readRootGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  readRootGet$Response(params?: {},
                       context?: HttpContext
  ): Observable<StrictHttpResponse<any>> {

    const rb = new RequestBuilder(this.rootUrl, ApiService.ReadRootGetPath, 'get');
    if (params) {
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<any>;
      })
    );
  }

  /**
   * Read Root.
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `readRootGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  readRootGet(params?: {},
              context?: HttpContext
  ): Observable<any> {

    return this.readRootGet$Response(params, context).pipe(
        map((r: StrictHttpResponse<any>) => r.body as any)
    );
  }

}
