/* tslint:disable */
/* eslint-disable */
import {Injectable} from '@angular/core';
import {HttpClient, HttpContext, HttpResponse} from '@angular/common/http';
import {BaseService} from '../base-service';
import {ApiConfiguration} from '../api-configuration';
import {StrictHttpResponse} from '../strict-http-response';
import {RequestBuilder} from '../request-builder';
import {Observable} from 'rxjs';
import {filter, map} from 'rxjs/operators';

import {CreateUpdatePlanning} from '../models/create-update-planning';
import {PlanningDto} from '../models/planning-dto';

@Injectable({
  providedIn: 'root',
})
export class PlanningService extends BaseService {
  constructor(
    config: ApiConfiguration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Path part for operation getPlanningPlanningPlanningIdGet
   */
  static readonly GetPlanningPlanningPlanningIdGetPath = '/planning/{planning_id}';
  /**
   * Path part for operation updatePlanningPlanningPlanningIdPut
   */
  static readonly UpdatePlanningPlanningPlanningIdPutPath = '/planning/{planning_id}';
  /**
   * Path part for operation getPlanningsPlanningGet
   */
  static readonly GetPlanningsPlanningGetPath = '/planning';
  /**
   * Path part for operation createPlanningPlanningPost
   */
  static readonly CreatePlanningPlanningPostPath = '/planning';

  /**
   * Get Planning.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `getPlanningPlanningPlanningIdGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  getPlanningPlanningPlanningIdGet$Response(params: {
                                              planning_id: number;
                                            },
                                            context?: HttpContext
  ): Observable<StrictHttpResponse<PlanningDto>> {

    const rb = new RequestBuilder(this.rootUrl, PlanningService.GetPlanningPlanningPlanningIdGetPath, 'get');
    if (params) {
      rb.path('planning_id', params.planning_id, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<PlanningDto>;
      })
    );
  }

  /**
   * Get Planning.
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `getPlanningPlanningPlanningIdGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  getPlanningPlanningPlanningIdGet(params: {
                                     planning_id: number;
                                   },
                                   context?: HttpContext
  ): Observable<PlanningDto> {

    return this.getPlanningPlanningPlanningIdGet$Response(params, context).pipe(
      map((r: StrictHttpResponse<PlanningDto>) => r.body as PlanningDto)
    );
  }

  /**
   * Update Planning.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `updatePlanningPlanningPlanningIdPut()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  updatePlanningPlanningPlanningIdPut$Response(params: {
                                                 planning_id: number;
                                                 body: CreateUpdatePlanning
                                               },
                                               context?: HttpContext
  ): Observable<StrictHttpResponse<PlanningDto>> {

    const rb = new RequestBuilder(this.rootUrl, PlanningService.UpdatePlanningPlanningPlanningIdPutPath, 'put');
    if (params) {
      rb.path('planning_id', params.planning_id, {});
      rb.body(params.body, 'application/json');
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<PlanningDto>;
      })
    );
  }

  /**
   * Update Planning.
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `updatePlanningPlanningPlanningIdPut$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  updatePlanningPlanningPlanningIdPut(params: {
                                        planning_id: number;
                                        body: CreateUpdatePlanning
                                      },
                                      context?: HttpContext
  ): Observable<PlanningDto> {

    return this.updatePlanningPlanningPlanningIdPut$Response(params, context).pipe(
      map((r: StrictHttpResponse<PlanningDto>) => r.body as PlanningDto)
    );
  }

  /**
   * Get Plannings.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `getPlanningsPlanningGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  getPlanningsPlanningGet$Response(params?: {
                                     skip?: number;
                                     limit?: number;
                                   },
                                   context?: HttpContext
  ): Observable<StrictHttpResponse<Array<PlanningDto>>> {

    const rb = new RequestBuilder(this.rootUrl, PlanningService.GetPlanningsPlanningGetPath, 'get');
    if (params) {
      rb.query('skip', params.skip, {});
      rb.query('limit', params.limit, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<Array<PlanningDto>>;
      })
    );
  }

  /**
   * Get Plannings.
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `getPlanningsPlanningGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  getPlanningsPlanningGet(params?: {
                            skip?: number;
                            limit?: number;
                          },
                          context?: HttpContext
  ): Observable<Array<PlanningDto>> {

    return this.getPlanningsPlanningGet$Response(params, context).pipe(
      map((r: StrictHttpResponse<Array<PlanningDto>>) => r.body as Array<PlanningDto>)
    );
  }

  /**
   * Create Planning.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `createPlanningPlanningPost()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  createPlanningPlanningPost$Response(params: {
                                        body: CreateUpdatePlanning
                                      },
                                      context?: HttpContext
  ): Observable<StrictHttpResponse<PlanningDto>> {

    const rb = new RequestBuilder(this.rootUrl, PlanningService.CreatePlanningPlanningPostPath, 'post');
    if (params) {
      rb.body(params.body, 'application/json');
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json',
      context: context
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<PlanningDto>;
      })
    );
  }

  /**
   * Create Planning.
   *
   *
   *
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `createPlanningPlanningPost$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  createPlanningPlanningPost(params: {
                               body: CreateUpdatePlanning
                             },
                             context?: HttpContext
  ): Observable<PlanningDto> {

    return this.createPlanningPlanningPost$Response(params, context).pipe(
      map((r: StrictHttpResponse<PlanningDto>) => r.body as PlanningDto)
    );
  }

}
