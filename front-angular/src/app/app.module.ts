import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {MatListModule} from '@angular/material/list';
import {MatTableModule} from '@angular/material/table';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {RecipeListComponent} from './recipes/recipe-list/recipe-list.component';
import {ApiJavaModule} from './api/java-api-client/api-java.module';
import {ApiPythonModule} from './api/python-api-client/api-python.module';
import {HttpClientModule} from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {RecipeAddEditComponent} from './recipes/recipe-add-edit/recipe-add-edit.component';
import {MatIconModule} from "@angular/material/icon";
import {MatButtonModule} from "@angular/material/button";
import {MatTooltipModule} from "@angular/material/tooltip";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import {FlexLayoutModule} from "@angular/flex-layout";
import {MatSnackBarModule} from "@angular/material/snack-bar";
import {RecipeDisplayComponent} from './recipes/recipe-display/recipe-display.component';
import {MatAutocompleteModule} from "@angular/material/autocomplete";
import {MatSelectModule} from "@angular/material/select";
import {IngredientManageComponent} from './recipes/ingredient-manage/ingredient-manage.component';
import {MatCardModule} from "@angular/material/card";
import {MatGridListModule} from "@angular/material/grid-list";
import {MatSidenavModule} from "@angular/material/sidenav";
import {MatChipsModule} from "@angular/material/chips";
import {AutocompleteFilterComponent} from './components/autocomplete-filter/autocomplete-filter.component';
import {PlanningListComponent} from './plannings/planning-list/planning-list.component';
import {PlanningAddEditComponent} from './plannings/planning-add-edit/planning-add-edit.component';
import {MatDialogModule} from "@angular/material/dialog";
import {RecipeChooserComponent} from "./recipes/recipe-chooser/recipe-chooser.component";
import {IngredientListComponent} from './ingredient-list/ingredient-list.component';

@NgModule({
  declarations: [
    AppComponent,
    RecipeListComponent,
    RecipeChooserComponent,
    RecipeAddEditComponent,
    RecipeDisplayComponent,
    IngredientManageComponent,
    AutocompleteFilterComponent,
    PlanningListComponent,
    PlanningAddEditComponent,
    IngredientListComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MatListModule,
    MatTableModule,
    MatDialogModule,
    HttpClientModule,
    ApiJavaModule.forRoot({rootUrl: 'api-java'}),
    ApiPythonModule.forRoot({rootUrl: 'api-python'}),
    BrowserAnimationsModule,
    MatIconModule,
    MatButtonModule,
    MatTooltipModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    FlexLayoutModule,
    MatSnackBarModule,
    MatAutocompleteModule,
    MatSelectModule,
    MatCardModule,
    MatGridListModule,
    MatSidenavModule,
    MatChipsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
