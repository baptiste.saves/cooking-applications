import {Component, Input, OnInit} from '@angular/core';
import {MeasureDto} from "../api/java-api-client/models/measure-dto";

@Component({
  selector: 'app-ingredient-list',
  templateUrl: './ingredient-list.component.html',
  styleUrls: ['./ingredient-list.component.scss']
})
export class IngredientListComponent implements OnInit {
  _measures: MeasureDto[]

  constructor() {
  }

  ngOnInit(): void {
  }

  @Input() set measures(measures: MeasureDto[]) {
    if (measures != null) {
      const accumulator: Map<String, MeasureDto> = new Map([]);
      measures.forEach((measure: MeasureDto) => {
        const key = measure.ingredientId + measure.unitId
        if (accumulator.get(key) === undefined) {
          accumulator.set(key, measure)
        } else {
          accumulator.get(key).quantity += measure.quantity
        }
      })
      this._measures = Array.from(accumulator.values()).sort((a, b) => a.ingredient.localeCompare(b.ingredient))
    }
  }

}
