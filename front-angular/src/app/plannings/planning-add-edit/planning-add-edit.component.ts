import {Component, OnInit} from '@angular/core';
import {FormArray, FormBuilder, Validators} from "@angular/forms";
import {PlanningService} from "../../api/python-api-client/services/planning.service";
import {MatSnackBar} from "@angular/material/snack-bar";
import {ActivatedRoute, Router} from "@angular/router";
import {firstValueFrom, from, mergeMap, toArray} from "rxjs";
import {MatDialog} from "@angular/material/dialog";
import {RecipeChooserComponent} from "../../recipes/recipe-chooser/recipe-chooser.component";
import {RecipeListDto} from "../../api/java-api-client/models/recipe-list-dto";
import {CreateUpdatePlanning} from "../../api/python-api-client/models/create-update-planning";
import {RecipeControllerService} from "../../api/java-api-client/services/recipe-controller.service";
import {PictureControllerService} from "../../api/java-api-client/services/picture-controller.service";
import {DomSanitizer} from "@angular/platform-browser";
import {MeasureDto} from "../../api/java-api-client/models/measure-dto";

@Component({
  selector: 'app-planning-add-edit',
  templateUrl: './planning-add-edit.component.html',
  styleUrls: ['./planning-add-edit.component.scss']
})
export class PlanningAddEditComponent implements OnInit {
  planningForm = this.formBuilder.group({
    title: ['', Validators.required],
    menus: this.formBuilder.array([])
  });
  private id: number;
  isAddMode: boolean;
  numberOfCols: any = 5;
  measures: MeasureDto[];
  private allRecipe: RecipeListDto[];

  constructor(private formBuilder: FormBuilder,
              private planningService: PlanningService,
              private snackBar: MatSnackBar,
              private route: ActivatedRoute,
              private router: Router,
              private matDialog: MatDialog,
              private recipeControllerService: RecipeControllerService,
              private pictureControllerService: PictureControllerService,
              private sanitizer: DomSanitizer,
  ) {
  }

  get menus() {
    return this.planningForm.controls["menus"] as FormArray;
  }

  async ngOnInit() {
    this.updateNumberOfCols(window.innerWidth);
    this.id = this.route.snapshot.params['id'];
    this.isAddMode = !this.id;
    this.allRecipe = await firstValueFrom(this.recipeControllerService.getRecipes())
    if (!this.isAddMode) {
      let planning = await firstValueFrom(this.planningService.getPlanningPlanningPlanningIdGet({planning_id: this.id}))
      this.planningForm.patchValue(planning)
      planning.menus.forEach(menuDto => {
        let recipe = this.allRecipe.find(value1 => value1.id == menuDto.recipe_id);
        this.addRecipe(menuDto.title, recipe)
        this.pictureControllerService.getPicture({id: recipe.picture}).subscribe(blob => {
          // @ts-ignore
          const objectURL = URL.createObjectURL(blob);
          recipe['pictureBlob'] = this.sanitizer.bypassSecurityTrustUrl(objectURL);
        })
      })
      this.calculateIngredients()
    }
  }

  calculateIngredients() {
    from(this.menus
      .controls
      .map(value => (value as FormArray).controls['recipe'].value)
    )
      .pipe(
        mergeMap((recipe) => this.recipeControllerService.getRecipe({id: recipe.id})),
        mergeMap(value => value.measures),
        toArray()
      )
      .subscribe(value => this.measures = value)
  }

  onSubmit() {
    let value = this.planningForm.value;
    let body: CreateUpdatePlanning = {
      title: value.title, menus: value.menus.map(recipe => {
        return {
          title: recipe['title'],
          recipe_id: recipe['recipe_id']
        }
      })
    };
    if (this.isAddMode) {
      this.planningService.createPlanningPlanningPost({body}).subscribe(value => {
        this.snackBar.open('Planning créée', 'ok', {duration: 3000});
        this.router.navigateByUrl('/planning/' + value.id)
      })
    } else {
      this.planningService.updatePlanningPlanningPlanningIdPut({planning_id: this.id, body})
        .subscribe(value => {
          this.snackBar.open('Planning mis à jour', 'ok', {duration: 3000});
          this.router.navigateByUrl('/planning/' + value.id)
        })
    }
  }

  chooseRecipe() {
    this.matDialog.open(RecipeChooserComponent, {
      "width": '6000px',
      "maxHeight": '90vh',
    }).afterClosed().subscribe(recipe => {
      this.addRecipe("", recipe);
    });
  }

  onRecipeSelected(recipe: RecipeListDto) {
    this.router.navigateByUrl("/recipe/" + recipe.id)
  }

  onResize(event) {
    this.updateNumberOfCols(event.target.innerWidth);
  }

  private updateNumberOfCols(width: number) {
    this.numberOfCols = Math.trunc(width / 280);
  }

  private addRecipe(title, recipe) {
    const menu = this.formBuilder.group({
      title: [title, Validators.required],
      recipe_id: [recipe.id, Validators.required],
      recipe: [recipe, Validators.required],
    })
    this.menus.push(menu)
    this.calculateIngredients()
  }
}
