import {Component, OnInit} from '@angular/core';
import {PlanningService} from "../../api/python-api-client/services/planning.service";
import {Router} from "@angular/router";
import {PlanningDto} from "../../api/python-api-client/models/planning-dto";

@Component({
  selector: 'app-planning-list',
  templateUrl: './planning-list.component.html',
  styleUrls: ['./planning-list.component.scss']
})
export class PlanningListComponent implements OnInit {
  datasource: PlanningDto[];

  constructor(private planningService: PlanningService,
              private router: Router
  ) {
  }

  ngOnInit(): void {
    this.planningService.getPlanningsPlanningGet().subscribe(value => {
      this.datasource = value
    })
  }

  onClikRow(row) {
    this.router.navigateByUrl('/planning/' + row.id)
  }
}
