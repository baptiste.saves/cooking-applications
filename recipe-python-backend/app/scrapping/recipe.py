from fastapi import APIRouter

from app.scrapping.recipe_scrapping_service import ScrappedRecipeDto, scrap_recipe

router = APIRouter(
    tags=["recipe"]
)


@router.get("/recipe/scrapping/", response_model=ScrappedRecipeDto)
async def scrap_recipe_from_another_site(url: str):
    return scrap_recipe(url)
