import re
import uuid

import Levenshtein
from java_api_python_client import ApiClient, IngredientControllerApi, IngredientDto
from java_api_python_client.configuration import Configuration
from pydantic.main import BaseModel
from recipe_scrapers import scrape_me

from app.config import settings

configuration = Configuration()
configuration.host = settings.java_api_url
client = ApiClient(configuration)
ingredientApi = IngredientControllerApi(client)

unit_regex = None


class PossibleIngredientDto(BaseModel):
    id: uuid.UUID | None = None
    quantity: float | None = None
    unit: uuid.UUID | None = None
    scraped_text: str


class ScrappedRecipeDto(BaseModel):
    title: str
    url: str
    picture: str
    yield_quantity: int
    ingredients: list[PossibleIngredientDto]
    instructions: list[str]


def __extract_quantity(ing: str) -> str | None:
    # We take the x first digit characters
    quantity = re.findall(r"^([\d.]*) .*$", ing)
    return quantity[0] if quantity else None


def __extract_unit(ing):
    # We try to match an unit --> ml, dl, l, mg, dg, g …
    # Guess we need to dl the list of possible units
    unit = re.findall(r"^({}) .*$".format(unit_regex), ing)
    return unit[0] if unit else None


def __remove_link_words(ing: str) -> str:
    return re.sub('^ (de |a |d\')', ' ', ing).strip()


def __chose_ingredient(ingredients: list[IngredientDto], ing):
    if not ingredients:
        return None
    if len(ingredients) == 1:
        return ingredients[0]
    print("Determining ingredient {}".format(ing))
    [(print("jaro_wrinkler {} {}".format(Levenshtein.jaro_winkler(ing, ingre.name), ingre.name), i, ingre.name)) for
     i, ingre
     in enumerate(ingredients)]
    ingredients_ = [(Levenshtein.distance(ing, ingre.name), i, ingre.name) for i, ingre in enumerate(ingredients)]
    for x in ingredients_:
        print(x[2], x[0])
    (dist, index, ingr) = min(ingredients_)
    print("Chosen {}".format(ingr))
    return ingredients[index]


def __identify_ingredient(ing: str):
    init_ing = ing
    quantity = __extract_quantity(ing)
    unit = "Unit"
    if quantity is not None:
        ing = ing[len(quantity) + 1::]
        extracted_unit = __extract_unit(ing)
        unit = extracted_unit if extracted_unit else unit
        if unit != 'Unit':
            ing = ing[len(unit)::]

    ing = __remove_link_words(ing)
    ingredients = ingredientApi.get_ingredients(name=ing)
    chosen_ingredient = __chose_ingredient(ingredients, ing)
    unit_id = None
    if chosen_ingredient:
        found_unit = [unit_ for unit_ in chosen_ingredient.units if unit_.name == unit]
        if found_unit:
            unit_id = found_unit[0].id

    return PossibleIngredientDto(
        id=chosen_ingredient.id if chosen_ingredient else None,
        unit=unit_id,
        quantity=float(quantity) if quantity else None,
        scraped_text=init_ing
    )


def scrap_recipe(url: str) -> ScrappedRecipeDto:
    global unit_regex
    if unit_regex is None:
        units = ingredientApi.get_all_units()
        unit_regex = '|'.join([unit.name for unit in units])

    scraper = scrape_me(url)
    ingredients = [__identify_ingredient(ing) for ing in scraper.ingredients()]
    yield_quantity = scraper.yields().split()[0]
    return ScrappedRecipeDto(
        title=scraper.title(),
        url=scraper.url,
        ingredients=ingredients,
        picture=scraper.image(),
        yield_quantity=yield_quantity,
        instructions=scraper.instructions_list()
    )
