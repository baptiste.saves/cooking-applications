from pydantic_settings import BaseSettings


class Settings(BaseSettings):
    java_api_url: str = "http://localhost:10000"


settings = Settings()
