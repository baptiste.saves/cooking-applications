from datetime import datetime
from typing import List

from sqlalchemy import ForeignKey, func
from sqlalchemy import String
from sqlalchemy.orm import Mapped
from sqlalchemy.orm import mapped_column
from sqlalchemy.orm import relationship

from app.planning.database import Base


class Planning(Base):
    __tablename__ = "plannings"

    id: Mapped[int] = mapped_column(primary_key=True, index=True, autoincrement=True)
    created_date: Mapped[datetime] = mapped_column(server_default=func.now(), nullable=False)
    title: Mapped[str] = mapped_column(String(512), nullable=False)
    menus: Mapped[List["Menu"]] = relationship(cascade="all, delete-orphan")


class Menu(Base):
    __tablename__ = "menus"

    id: Mapped[int] = mapped_column(primary_key=True, index=True, autoincrement=True)
    title: Mapped[str] = mapped_column(nullable=False)
    recipe_id: Mapped[str] = mapped_column(nullable=False)
    planning_id: Mapped[int] = mapped_column(ForeignKey("plannings.id"))
