from datetime import datetime

from fastapi import APIRouter, Depends, HTTPException
from pydantic import BaseModel, ConfigDict
from sqlalchemy.orm import Session

from app.planning.database import get_db
from app.planning.models import Planning, Menu
from app.planning.planning_repository import PlanningRepository

router = APIRouter(
    tags=["planning"]
)


class MenuDto(BaseModel):
    model_config = ConfigDict(from_attributes=True)
    title: str
    recipe_id: str




class CreateUpdatePlanning(BaseModel):
    model_config = ConfigDict(from_attributes=True)
    title: str
    menus: list[MenuDto]


class PlanningDto(CreateUpdatePlanning):
    model_config = ConfigDict(from_attributes=True)
    id: int
    created_date: datetime




@router.get("/planning/{planning_id}", response_model=PlanningDto)
def get_planning(planning_id: int, session: Session = Depends(get_db)):
    planning = PlanningRepository.get_planning(planning_id, session)
    if planning is None:
        raise HTTPException(status_code=404, detail="User not found")
    return planning


@router.get("/planning", response_model=list[PlanningDto])
def get_plannings(session: Session = Depends(get_db), skip: int = 0, limit: int = 100):
    return PlanningRepository.get_plannings(skip=skip, limit=limit, session=session)


@router.put("/planning/{planning_id}", response_model=PlanningDto)
def update_planning(planning_id: int, planning_update: CreateUpdatePlanning, session: Session = Depends(get_db)):
    planning: Planning = PlanningRepository.get_planning(planning_id, session)
    if planning is None:
        raise HTTPException(status_code=404, detail="User not found")
    planning.title = planning_update.title
    planning.menus = [Menu(title=item.title, recipe_id=item.recipe_id) for item in planning_update.menus]

    session.commit()
    session.refresh(planning)
    return PlanningDto.model_validate(planning)


@router.post("/planning", response_model=PlanningDto)
def create_planning(planning_dto: CreateUpdatePlanning, db: Session = Depends(get_db)):
    db_planning = Planning(title=planning_dto.title,
                           menus=[Menu(title=item.title, recipe_id=item.recipe_id) for item in
                                  planning_dto.menus])
    db.add(db_planning)
    db.commit()
    db.refresh(db_planning)
    return PlanningDto.model_validate(db_planning)
