from sqlalchemy import select
from sqlalchemy.orm import Session

from app.planning.models import Planning


class PlanningRepository:

    @staticmethod
    def get_planning(planning_id: int, session: Session) -> Planning | None:
        return session.scalar(select(Planning).filter(Planning.id == planning_id))

    @staticmethod
    def get_plannings(skip: int, limit: int, session: Session) -> list[Planning]:
        return session.query(Planning).offset(skip).limit(limit).all()
