from fastapi import FastAPI

from app.scrapping import recipe
from app.planning import planning_router

app = FastAPI()
app.include_router(recipe.router)
app.include_router(planning_router.router)


@app.get("/")
def read_root():
    return {"Hello": "World"}
