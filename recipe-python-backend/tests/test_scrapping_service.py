import unittest
from unittest import mock
from unittest.mock import MagicMock
from uuid import uuid4

from java_api_python_client import IngredientDto, UnitDto, IngredientTypeEnum

from app.scrapping import recipe_scrapping_service
from app.scrapping.recipe_scrapping_service import scrap_recipe


class TestScrappingService(unittest.TestCase):
    @mock.patch('app.service.recipe_scrapping_service.ingredientApi')
    @mock.patch('app.service.recipe_scrapping_service.scrape_me')
    def test_scrapping(self, scrapper_mock: mock.MagicMock, ingredient_api_mock: mock.MagicMock):
        scrapped_mock = MagicMock()
        scrapper_mock.return_value = scrapped_mock

        recipe_scrapping_service.unit_regex = 'Unit|noix|ml|cl|dl|l|mg|g|kg'
        scrapped_mock.ingredients.return_value = ['400 g de farine', '250 g de beurre', '1 oeufs', '230 g de sucre',
                                                  '500 g de ricotta', 'zeste de citron râpé', '100 g de chocolat',
                                                  "1 cuillères d'extrait de vanille ou de vanille en poudre (facultatif)"]

        scrapped_mock.yields.return_value = "4 people"
        scrapped_mock.title.return_value = "title"
        scrapped_mock.url = "url"
        scrapped_mock.image.return_value = "image"
        scrapped_mock.instructions_list.return_value = ["1", "2"]

        ingredient_id = uuid4()
        ingredient_api_mock.get_ingredients.return_value = [IngredientDto(
            id=str(ingredient_id),
            name="ingredient",
            units=[UnitDto(id=str(uuid4()), name="litre")],
            types=[IngredientTypeEnum.CHICKEN]
        )]

        assert scrap_recipe("https://test.recipe").model_dump() == {'title': 'title',
                                                                    'url': 'url',
                                                                    'picture': 'image',
                                                                    'yield_quantity': 4,
                                                                    'ingredients': [
                                                                        {'id': ingredient_id,
                                                                         'quantity': 400.0, 'unit': None,
                                                                         'scraped_text': '400 g de farine'},
                                                                        {'id': ingredient_id,
                                                                         'quantity': 250.0, 'unit': None,
                                                                         'scraped_text': '250 g de beurre'},
                                                                        {'id': ingredient_id,
                                                                         'quantity': 1.0, 'unit': None,
                                                                         'scraped_text': '1 oeufs'},
                                                                        {'id': ingredient_id,
                                                                         'quantity': 230.0, 'unit': None,
                                                                         'scraped_text': '230 g de sucre'},
                                                                        {'id': ingredient_id,
                                                                         'quantity': 500.0, 'unit': None,
                                                                         'scraped_text': '500 g de ricotta'},
                                                                        {'id': ingredient_id,
                                                                         'quantity': None, 'unit': None,
                                                                         'scraped_text': 'zeste de citron râpé'},
                                                                        {'id': ingredient_id,
                                                                         'quantity': 100.0, 'unit': None,
                                                                         'scraped_text': '100 g de chocolat'},
                                                                        {'id': ingredient_id,
                                                                         'quantity': 1.0, 'unit': None,
                                                                         'scraped_text': "1 cuillères d'extrait de vanille ou de vanille en poudre (facultatif)"}],
                                                                    'instructions': ['1', '2']}
