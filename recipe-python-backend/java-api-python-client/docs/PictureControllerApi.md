# java_api_python_client.PictureControllerApi

All URIs are relative to *http://localhost:10000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_picture**](PictureControllerApi.md#get_picture) | **GET** /picture/{id} | 
[**upload_picture**](PictureControllerApi.md#upload_picture) | **POST** /picture | 


# **get_picture**
> bytearray get_picture(id)



### Example


```python
import java_api_python_client
from java_api_python_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost:10000
# See configuration.py for a list of all supported configuration parameters.
configuration = java_api_python_client.Configuration(
    host = "http://localhost:10000"
)


# Enter a context with an instance of the API client
with java_api_python_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = java_api_python_client.PictureControllerApi(api_client)
    id = 'id_example' # str | 

    try:
        api_response = api_instance.get_picture(id)
        print("The response of PictureControllerApi->get_picture:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling PictureControllerApi->get_picture: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**|  | 

### Return type

**bytearray**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/octet-stream

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**0** | default response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **upload_picture**
> str upload_picture(picture)



### Example


```python
import java_api_python_client
from java_api_python_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost:10000
# See configuration.py for a list of all supported configuration parameters.
configuration = java_api_python_client.Configuration(
    host = "http://localhost:10000"
)


# Enter a context with an instance of the API client
with java_api_python_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = java_api_python_client.PictureControllerApi(api_client)
    picture = None # bytearray | 

    try:
        api_response = api_instance.upload_picture(picture)
        print("The response of PictureControllerApi->upload_picture:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling PictureControllerApi->upload_picture: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **picture** | **bytearray**|  | 

### Return type

**str**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | Created |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

