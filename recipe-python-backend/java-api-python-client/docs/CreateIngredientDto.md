# CreateIngredientDto


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** |  | 

## Example

```python
from java_api_python_client.models.create_ingredient_dto import CreateIngredientDto

# TODO update the JSON string below
json = "{}"
# create an instance of CreateIngredientDto from a JSON string
create_ingredient_dto_instance = CreateIngredientDto.from_json(json)
# print the JSON string representation of the object
print(CreateIngredientDto.to_json())

# convert the object into a dict
create_ingredient_dto_dict = create_ingredient_dto_instance.to_dict()
# create an instance of CreateIngredientDto from a dict
create_ingredient_dto_from_dict = CreateIngredientDto.from_dict(create_ingredient_dto_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


