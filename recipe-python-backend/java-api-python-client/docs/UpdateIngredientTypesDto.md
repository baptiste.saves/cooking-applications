# UpdateIngredientTypesDto


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**types** | [**List[IngredientTypeEnum]**](IngredientTypeEnum.md) |  | 

## Example

```python
from java_api_python_client.models.update_ingredient_types_dto import UpdateIngredientTypesDto

# TODO update the JSON string below
json = "{}"
# create an instance of UpdateIngredientTypesDto from a JSON string
update_ingredient_types_dto_instance = UpdateIngredientTypesDto.from_json(json)
# print the JSON string representation of the object
print(UpdateIngredientTypesDto.to_json())

# convert the object into a dict
update_ingredient_types_dto_dict = update_ingredient_types_dto_instance.to_dict()
# create an instance of UpdateIngredientTypesDto from a dict
update_ingredient_types_dto_from_dict = UpdateIngredientTypesDto.from_dict(update_ingredient_types_dto_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


