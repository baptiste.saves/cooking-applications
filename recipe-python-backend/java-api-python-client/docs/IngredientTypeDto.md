# IngredientTypeDto


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**types** | [**List[IngredientTypeEnum]**](IngredientTypeEnum.md) |  | [optional] 

## Example

```python
from java_api_python_client.models.ingredient_type_dto import IngredientTypeDto

# TODO update the JSON string below
json = "{}"
# create an instance of IngredientTypeDto from a JSON string
ingredient_type_dto_instance = IngredientTypeDto.from_json(json)
# print the JSON string representation of the object
print(IngredientTypeDto.to_json())

# convert the object into a dict
ingredient_type_dto_dict = ingredient_type_dto_instance.to_dict()
# create an instance of IngredientTypeDto from a dict
ingredient_type_dto_from_dict = IngredientTypeDto.from_dict(ingredient_type_dto_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


