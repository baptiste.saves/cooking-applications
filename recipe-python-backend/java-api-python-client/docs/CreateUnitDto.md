# CreateUnitDto


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** |  | 

## Example

```python
from java_api_python_client.models.create_unit_dto import CreateUnitDto

# TODO update the JSON string below
json = "{}"
# create an instance of CreateUnitDto from a JSON string
create_unit_dto_instance = CreateUnitDto.from_json(json)
# print the JSON string representation of the object
print(CreateUnitDto.to_json())

# convert the object into a dict
create_unit_dto_dict = create_unit_dto_instance.to_dict()
# create an instance of CreateUnitDto from a dict
create_unit_dto_from_dict = CreateUnitDto.from_dict(create_unit_dto_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


