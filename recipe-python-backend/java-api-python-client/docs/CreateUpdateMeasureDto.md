# CreateUpdateMeasureDto


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ingredient_id** | **str** |  | 
**unit_id** | **str** |  | 
**quantity** | **float** |  | 

## Example

```python
from java_api_python_client.models.create_update_measure_dto import CreateUpdateMeasureDto

# TODO update the JSON string below
json = "{}"
# create an instance of CreateUpdateMeasureDto from a JSON string
create_update_measure_dto_instance = CreateUpdateMeasureDto.from_json(json)
# print the JSON string representation of the object
print(CreateUpdateMeasureDto.to_json())

# convert the object into a dict
create_update_measure_dto_dict = create_update_measure_dto_instance.to_dict()
# create an instance of CreateUpdateMeasureDto from a dict
create_update_measure_dto_from_dict = CreateUpdateMeasureDto.from_dict(create_update_measure_dto_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


