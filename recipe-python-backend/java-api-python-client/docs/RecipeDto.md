# RecipeDto


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** |  | 
**created_at** | **date** |  | 
**title** | **str** |  | 
**content** | **str** |  | 
**yield_quantity** | **int** |  | 
**measures** | [**List[MeasureDto]**](MeasureDto.md) |  | 
**picture_ids** | **List[str]** |  | [optional] 
**link** | **str** |  | [optional] 

## Example

```python
from java_api_python_client.models.recipe_dto import RecipeDto

# TODO update the JSON string below
json = "{}"
# create an instance of RecipeDto from a JSON string
recipe_dto_instance = RecipeDto.from_json(json)
# print the JSON string representation of the object
print(RecipeDto.to_json())

# convert the object into a dict
recipe_dto_dict = recipe_dto_instance.to_dict()
# create an instance of RecipeDto from a dict
recipe_dto_from_dict = RecipeDto.from_dict(recipe_dto_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


