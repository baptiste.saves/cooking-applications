# IngredientTypeEnum


## Enum

* `VEGETABLE` (value: `'VEGETABLE'`)

* `FRUIT` (value: `'FRUIT'`)

* `BEEF` (value: `'BEEF'`)

* `DUCK` (value: `'DUCK'`)

* `CHICKEN` (value: `'CHICKEN'`)

* `FISH` (value: `'FISH'`)

* `ALCOHOL` (value: `'ALCOHOL'`)

* `PASTA` (value: `'PASTA'`)

* `RICE` (value: `'RICE'`)

* `PORK` (value: `'PORK'`)

* `DAIRY` (value: `'DAIRY'`)

* `VEAL` (value: `'VEAL'`)

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


