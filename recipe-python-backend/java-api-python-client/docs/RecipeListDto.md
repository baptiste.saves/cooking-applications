# RecipeListDto


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** |  | [optional] 
**created_at** | **date** |  | [optional] 
**title** | **str** |  | [optional] 
**picture** | **str** |  | [optional] 

## Example

```python
from java_api_python_client.models.recipe_list_dto import RecipeListDto

# TODO update the JSON string below
json = "{}"
# create an instance of RecipeListDto from a JSON string
recipe_list_dto_instance = RecipeListDto.from_json(json)
# print the JSON string representation of the object
print(RecipeListDto.to_json())

# convert the object into a dict
recipe_list_dto_dict = recipe_list_dto_instance.to_dict()
# create an instance of RecipeListDto from a dict
recipe_list_dto_from_dict = RecipeListDto.from_dict(recipe_list_dto_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


