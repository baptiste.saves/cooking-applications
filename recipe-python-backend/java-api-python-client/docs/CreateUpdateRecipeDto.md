# CreateUpdateRecipeDto


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**title** | **str** |  | 
**content** | **str** |  | 
**yield_quantity** | **int** |  | 
**measures** | [**List[CreateUpdateMeasureDto]**](CreateUpdateMeasureDto.md) |  | 
**picture_ids** | **List[str]** |  | [optional] 
**link** | **str** |  | [optional] 

## Example

```python
from java_api_python_client.models.create_update_recipe_dto import CreateUpdateRecipeDto

# TODO update the JSON string below
json = "{}"
# create an instance of CreateUpdateRecipeDto from a JSON string
create_update_recipe_dto_instance = CreateUpdateRecipeDto.from_json(json)
# print the JSON string representation of the object
print(CreateUpdateRecipeDto.to_json())

# convert the object into a dict
create_update_recipe_dto_dict = create_update_recipe_dto_instance.to_dict()
# create an instance of CreateUpdateRecipeDto from a dict
create_update_recipe_dto_from_dict = CreateUpdateRecipeDto.from_dict(create_update_recipe_dto_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


