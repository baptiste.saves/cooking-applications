# java_api_python_client.RecipeControllerApi

All URIs are relative to *http://localhost:10000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**create_recipe**](RecipeControllerApi.md#create_recipe) | **POST** /recipe | 
[**get_recipe**](RecipeControllerApi.md#get_recipe) | **GET** /recipe/{id} | 
[**get_recipes**](RecipeControllerApi.md#get_recipes) | **GET** /recipe | 
[**update_recipe**](RecipeControllerApi.md#update_recipe) | **PUT** /recipe/{id} | 


# **create_recipe**
> str create_recipe(create_update_recipe_dto)



### Example


```python
import java_api_python_client
from java_api_python_client.models.create_update_recipe_dto import CreateUpdateRecipeDto
from java_api_python_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost:10000
# See configuration.py for a list of all supported configuration parameters.
configuration = java_api_python_client.Configuration(
    host = "http://localhost:10000"
)


# Enter a context with an instance of the API client
with java_api_python_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = java_api_python_client.RecipeControllerApi(api_client)
    create_update_recipe_dto = java_api_python_client.CreateUpdateRecipeDto() # CreateUpdateRecipeDto | 

    try:
        api_response = api_instance.create_recipe(create_update_recipe_dto)
        print("The response of RecipeControllerApi->create_recipe:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling RecipeControllerApi->create_recipe: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **create_update_recipe_dto** | [**CreateUpdateRecipeDto**](CreateUpdateRecipeDto.md)|  | 

### Return type

**str**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | Created |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_recipe**
> RecipeDto get_recipe(id)



### Example


```python
import java_api_python_client
from java_api_python_client.models.recipe_dto import RecipeDto
from java_api_python_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost:10000
# See configuration.py for a list of all supported configuration parameters.
configuration = java_api_python_client.Configuration(
    host = "http://localhost:10000"
)


# Enter a context with an instance of the API client
with java_api_python_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = java_api_python_client.RecipeControllerApi(api_client)
    id = 'id_example' # str | 

    try:
        api_response = api_instance.get_recipe(id)
        print("The response of RecipeControllerApi->get_recipe:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling RecipeControllerApi->get_recipe: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**|  | 

### Return type

[**RecipeDto**](RecipeDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_recipes**
> List[RecipeListDto] get_recipes(type_contains=type_contains, type_does_not_contain=type_does_not_contain)



### Example


```python
import java_api_python_client
from java_api_python_client.models.ingredient_type_enum import IngredientTypeEnum
from java_api_python_client.models.recipe_list_dto import RecipeListDto
from java_api_python_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost:10000
# See configuration.py for a list of all supported configuration parameters.
configuration = java_api_python_client.Configuration(
    host = "http://localhost:10000"
)


# Enter a context with an instance of the API client
with java_api_python_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = java_api_python_client.RecipeControllerApi(api_client)
    type_contains = [java_api_python_client.IngredientTypeEnum()] # List[IngredientTypeEnum] |  (optional)
    type_does_not_contain = [java_api_python_client.IngredientTypeEnum()] # List[IngredientTypeEnum] |  (optional)

    try:
        api_response = api_instance.get_recipes(type_contains=type_contains, type_does_not_contain=type_does_not_contain)
        print("The response of RecipeControllerApi->get_recipes:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling RecipeControllerApi->get_recipes: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **type_contains** | [**List[IngredientTypeEnum]**](IngredientTypeEnum.md)|  | [optional] 
 **type_does_not_contain** | [**List[IngredientTypeEnum]**](IngredientTypeEnum.md)|  | [optional] 

### Return type

[**List[RecipeListDto]**](RecipeListDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_recipe**
> str update_recipe(id, create_update_recipe_dto)



### Example


```python
import java_api_python_client
from java_api_python_client.models.create_update_recipe_dto import CreateUpdateRecipeDto
from java_api_python_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost:10000
# See configuration.py for a list of all supported configuration parameters.
configuration = java_api_python_client.Configuration(
    host = "http://localhost:10000"
)


# Enter a context with an instance of the API client
with java_api_python_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = java_api_python_client.RecipeControllerApi(api_client)
    id = 'id_example' # str | 
    create_update_recipe_dto = java_api_python_client.CreateUpdateRecipeDto() # CreateUpdateRecipeDto | 

    try:
        api_response = api_instance.update_recipe(id, create_update_recipe_dto)
        print("The response of RecipeControllerApi->update_recipe:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling RecipeControllerApi->update_recipe: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**|  | 
 **create_update_recipe_dto** | [**CreateUpdateRecipeDto**](CreateUpdateRecipeDto.md)|  | 

### Return type

**str**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

