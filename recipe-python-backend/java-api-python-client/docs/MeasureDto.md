# MeasureDto


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**quantity** | **float** |  | 
**ingredient** | **str** |  | 
**unit** | **str** |  | 
**ingredient_id** | **str** |  | 
**unit_id** | **str** |  | 

## Example

```python
from java_api_python_client.models.measure_dto import MeasureDto

# TODO update the JSON string below
json = "{}"
# create an instance of MeasureDto from a JSON string
measure_dto_instance = MeasureDto.from_json(json)
# print the JSON string representation of the object
print(MeasureDto.to_json())

# convert the object into a dict
measure_dto_dict = measure_dto_instance.to_dict()
# create an instance of MeasureDto from a dict
measure_dto_from_dict = MeasureDto.from_dict(measure_dto_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


