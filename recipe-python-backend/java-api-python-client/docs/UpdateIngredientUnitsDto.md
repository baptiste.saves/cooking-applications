# UpdateIngredientUnitsDto


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**units** | **List[str]** |  | 

## Example

```python
from java_api_python_client.models.update_ingredient_units_dto import UpdateIngredientUnitsDto

# TODO update the JSON string below
json = "{}"
# create an instance of UpdateIngredientUnitsDto from a JSON string
update_ingredient_units_dto_instance = UpdateIngredientUnitsDto.from_json(json)
# print the JSON string representation of the object
print(UpdateIngredientUnitsDto.to_json())

# convert the object into a dict
update_ingredient_units_dto_dict = update_ingredient_units_dto_instance.to_dict()
# create an instance of UpdateIngredientUnitsDto from a dict
update_ingredient_units_dto_from_dict = UpdateIngredientUnitsDto.from_dict(update_ingredient_units_dto_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


