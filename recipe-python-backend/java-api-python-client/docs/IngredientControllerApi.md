# java_api_python_client.IngredientControllerApi

All URIs are relative to *http://localhost:10000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**create_ingredient**](IngredientControllerApi.md#create_ingredient) | **POST** /ingredient | 
[**create_unit**](IngredientControllerApi.md#create_unit) | **POST** /ingredient/unit | 
[**get_all_ingredient_types**](IngredientControllerApi.md#get_all_ingredient_types) | **GET** /ingredient/type | 
[**get_all_units**](IngredientControllerApi.md#get_all_units) | **GET** /ingredient/unit | 
[**get_ingredient**](IngredientControllerApi.md#get_ingredient) | **GET** /ingredient/{id} | 
[**get_ingredients**](IngredientControllerApi.md#get_ingredients) | **GET** /ingredient | 
[**replace_ingredient_types**](IngredientControllerApi.md#replace_ingredient_types) | **PUT** /ingredient/{id}/type | 
[**replace_ingredient_units**](IngredientControllerApi.md#replace_ingredient_units) | **PUT** /ingredient/{id}/units | 


# **create_ingredient**
> IngredientDto create_ingredient(create_ingredient_dto)



### Example


```python
import java_api_python_client
from java_api_python_client.models.create_ingredient_dto import CreateIngredientDto
from java_api_python_client.models.ingredient_dto import IngredientDto
from java_api_python_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost:10000
# See configuration.py for a list of all supported configuration parameters.
configuration = java_api_python_client.Configuration(
    host = "http://localhost:10000"
)


# Enter a context with an instance of the API client
with java_api_python_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = java_api_python_client.IngredientControllerApi(api_client)
    create_ingredient_dto = java_api_python_client.CreateIngredientDto() # CreateIngredientDto | 

    try:
        api_response = api_instance.create_ingredient(create_ingredient_dto)
        print("The response of IngredientControllerApi->create_ingredient:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling IngredientControllerApi->create_ingredient: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **create_ingredient_dto** | [**CreateIngredientDto**](CreateIngredientDto.md)|  | 

### Return type

[**IngredientDto**](IngredientDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | Created |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_unit**
> UnitDto create_unit(create_unit_dto)



### Example


```python
import java_api_python_client
from java_api_python_client.models.create_unit_dto import CreateUnitDto
from java_api_python_client.models.unit_dto import UnitDto
from java_api_python_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost:10000
# See configuration.py for a list of all supported configuration parameters.
configuration = java_api_python_client.Configuration(
    host = "http://localhost:10000"
)


# Enter a context with an instance of the API client
with java_api_python_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = java_api_python_client.IngredientControllerApi(api_client)
    create_unit_dto = java_api_python_client.CreateUnitDto() # CreateUnitDto | 

    try:
        api_response = api_instance.create_unit(create_unit_dto)
        print("The response of IngredientControllerApi->create_unit:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling IngredientControllerApi->create_unit: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **create_unit_dto** | [**CreateUnitDto**](CreateUnitDto.md)|  | 

### Return type

[**UnitDto**](UnitDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | Created |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_all_ingredient_types**
> IngredientTypeDto get_all_ingredient_types()



### Example


```python
import java_api_python_client
from java_api_python_client.models.ingredient_type_dto import IngredientTypeDto
from java_api_python_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost:10000
# See configuration.py for a list of all supported configuration parameters.
configuration = java_api_python_client.Configuration(
    host = "http://localhost:10000"
)


# Enter a context with an instance of the API client
with java_api_python_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = java_api_python_client.IngredientControllerApi(api_client)

    try:
        api_response = api_instance.get_all_ingredient_types()
        print("The response of IngredientControllerApi->get_all_ingredient_types:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling IngredientControllerApi->get_all_ingredient_types: %s\n" % e)
```



### Parameters

This endpoint does not need any parameter.

### Return type

[**IngredientTypeDto**](IngredientTypeDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_all_units**
> List[UnitDto] get_all_units()



### Example


```python
import java_api_python_client
from java_api_python_client.models.unit_dto import UnitDto
from java_api_python_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost:10000
# See configuration.py for a list of all supported configuration parameters.
configuration = java_api_python_client.Configuration(
    host = "http://localhost:10000"
)


# Enter a context with an instance of the API client
with java_api_python_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = java_api_python_client.IngredientControllerApi(api_client)

    try:
        api_response = api_instance.get_all_units()
        print("The response of IngredientControllerApi->get_all_units:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling IngredientControllerApi->get_all_units: %s\n" % e)
```



### Parameters

This endpoint does not need any parameter.

### Return type

[**List[UnitDto]**](UnitDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_ingredient**
> IngredientDto get_ingredient(id)



### Example


```python
import java_api_python_client
from java_api_python_client.models.ingredient_dto import IngredientDto
from java_api_python_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost:10000
# See configuration.py for a list of all supported configuration parameters.
configuration = java_api_python_client.Configuration(
    host = "http://localhost:10000"
)


# Enter a context with an instance of the API client
with java_api_python_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = java_api_python_client.IngredientControllerApi(api_client)
    id = 'id_example' # str | 

    try:
        api_response = api_instance.get_ingredient(id)
        print("The response of IngredientControllerApi->get_ingredient:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling IngredientControllerApi->get_ingredient: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**|  | 

### Return type

[**IngredientDto**](IngredientDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_ingredients**
> List[IngredientDto] get_ingredients(name=name)



### Example


```python
import java_api_python_client
from java_api_python_client.models.ingredient_dto import IngredientDto
from java_api_python_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost:10000
# See configuration.py for a list of all supported configuration parameters.
configuration = java_api_python_client.Configuration(
    host = "http://localhost:10000"
)


# Enter a context with an instance of the API client
with java_api_python_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = java_api_python_client.IngredientControllerApi(api_client)
    name = 'name_example' # str |  (optional)

    try:
        api_response = api_instance.get_ingredients(name=name)
        print("The response of IngredientControllerApi->get_ingredients:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling IngredientControllerApi->get_ingredients: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **str**|  | [optional] 

### Return type

[**List[IngredientDto]**](IngredientDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **replace_ingredient_types**
> IngredientDto replace_ingredient_types(id, update_ingredient_types_dto)



### Example


```python
import java_api_python_client
from java_api_python_client.models.ingredient_dto import IngredientDto
from java_api_python_client.models.update_ingredient_types_dto import UpdateIngredientTypesDto
from java_api_python_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost:10000
# See configuration.py for a list of all supported configuration parameters.
configuration = java_api_python_client.Configuration(
    host = "http://localhost:10000"
)


# Enter a context with an instance of the API client
with java_api_python_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = java_api_python_client.IngredientControllerApi(api_client)
    id = 'id_example' # str | 
    update_ingredient_types_dto = java_api_python_client.UpdateIngredientTypesDto() # UpdateIngredientTypesDto | 

    try:
        api_response = api_instance.replace_ingredient_types(id, update_ingredient_types_dto)
        print("The response of IngredientControllerApi->replace_ingredient_types:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling IngredientControllerApi->replace_ingredient_types: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**|  | 
 **update_ingredient_types_dto** | [**UpdateIngredientTypesDto**](UpdateIngredientTypesDto.md)|  | 

### Return type

[**IngredientDto**](IngredientDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **replace_ingredient_units**
> IngredientDto replace_ingredient_units(id, update_ingredient_units_dto)



### Example


```python
import java_api_python_client
from java_api_python_client.models.ingredient_dto import IngredientDto
from java_api_python_client.models.update_ingredient_units_dto import UpdateIngredientUnitsDto
from java_api_python_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost:10000
# See configuration.py for a list of all supported configuration parameters.
configuration = java_api_python_client.Configuration(
    host = "http://localhost:10000"
)


# Enter a context with an instance of the API client
with java_api_python_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = java_api_python_client.IngredientControllerApi(api_client)
    id = 'id_example' # str | 
    update_ingredient_units_dto = java_api_python_client.UpdateIngredientUnitsDto() # UpdateIngredientUnitsDto | 

    try:
        api_response = api_instance.replace_ingredient_units(id, update_ingredient_units_dto)
        print("The response of IngredientControllerApi->replace_ingredient_units:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling IngredientControllerApi->replace_ingredient_units: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**|  | 
 **update_ingredient_units_dto** | [**UpdateIngredientUnitsDto**](UpdateIngredientUnitsDto.md)|  | 

### Return type

[**IngredientDto**](IngredientDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

