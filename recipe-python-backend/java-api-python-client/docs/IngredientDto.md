# IngredientDto


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** |  | 
**name** | **str** |  | 
**units** | [**List[UnitDto]**](UnitDto.md) |  | 
**types** | [**List[IngredientTypeEnum]**](IngredientTypeEnum.md) |  | 

## Example

```python
from java_api_python_client.models.ingredient_dto import IngredientDto

# TODO update the JSON string below
json = "{}"
# create an instance of IngredientDto from a JSON string
ingredient_dto_instance = IngredientDto.from_json(json)
# print the JSON string representation of the object
print(IngredientDto.to_json())

# convert the object into a dict
ingredient_dto_dict = ingredient_dto_instance.to_dict()
# create an instance of IngredientDto from a dict
ingredient_dto_from_dict = IngredientDto.from_dict(ingredient_dto_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


