# coding: utf-8

"""
    OpenAPI definition

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)

    The version of the OpenAPI document: v0
    Generated by OpenAPI Generator (https://openapi-generator.tech)

    Do not edit the class manually.
"""  # noqa: E501


from __future__ import annotations
import pprint
import re  # noqa: F401
import json

from pydantic import BaseModel, ConfigDict, Field, StrictStr
from typing import Any, ClassVar, Dict, List, Optional
from typing_extensions import Annotated
from java_api_python_client.models.create_update_measure_dto import CreateUpdateMeasureDto
from typing import Optional, Set
from typing_extensions import Self

class CreateUpdateRecipeDto(BaseModel):
    """
    CreateUpdateRecipeDto
    """ # noqa: E501
    title: Annotated[str, Field(min_length=6, strict=True, max_length=200)]
    content: Annotated[str, Field(min_length=10, strict=True, max_length=3000)]
    yield_quantity: Annotated[int, Field(strict=True, ge=1)] = Field(alias="yieldQuantity")
    measures: List[CreateUpdateMeasureDto]
    picture_ids: Optional[List[StrictStr]] = Field(default=None, alias="pictureIds")
    link: Optional[StrictStr] = None
    __properties: ClassVar[List[str]] = ["title", "content", "yieldQuantity", "measures", "pictureIds", "link"]

    model_config = ConfigDict(
        populate_by_name=True,
        validate_assignment=True,
        protected_namespaces=(),
    )


    def to_str(self) -> str:
        """Returns the string representation of the model using alias"""
        return pprint.pformat(self.model_dump(by_alias=True))

    def to_json(self) -> str:
        """Returns the JSON representation of the model using alias"""
        # TODO: pydantic v2: use .model_dump_json(by_alias=True, exclude_unset=True) instead
        return json.dumps(self.to_dict())

    @classmethod
    def from_json(cls, json_str: str) -> Optional[Self]:
        """Create an instance of CreateUpdateRecipeDto from a JSON string"""
        return cls.from_dict(json.loads(json_str))

    def to_dict(self) -> Dict[str, Any]:
        """Return the dictionary representation of the model using alias.

        This has the following differences from calling pydantic's
        `self.model_dump(by_alias=True)`:

        * `None` is only added to the output dict for nullable fields that
          were set at model initialization. Other fields with value `None`
          are ignored.
        """
        excluded_fields: Set[str] = set([
        ])

        _dict = self.model_dump(
            by_alias=True,
            exclude=excluded_fields,
            exclude_none=True,
        )
        # override the default output from pydantic by calling `to_dict()` of each item in measures (list)
        _items = []
        if self.measures:
            for _item in self.measures:
                if _item:
                    _items.append(_item.to_dict())
            _dict['measures'] = _items
        return _dict

    @classmethod
    def from_dict(cls, obj: Optional[Dict[str, Any]]) -> Optional[Self]:
        """Create an instance of CreateUpdateRecipeDto from a dict"""
        if obj is None:
            return None

        if not isinstance(obj, dict):
            return cls.model_validate(obj)

        _obj = cls.model_validate({
            "title": obj.get("title"),
            "content": obj.get("content"),
            "yieldQuantity": obj.get("yieldQuantity"),
            "measures": [CreateUpdateMeasureDto.from_dict(_item) for _item in obj["measures"]] if obj.get("measures") is not None else None,
            "pictureIds": obj.get("pictureIds"),
            "link": obj.get("link")
        })
        return _obj


