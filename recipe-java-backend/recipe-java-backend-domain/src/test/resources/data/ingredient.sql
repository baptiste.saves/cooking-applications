insert into ingredient(id, created_at, name)
VALUES (X'c80d54d3500d453994798e8961477193', NOW(), 'creme'),
       (X'c80d54d3500d453994798e8961477194', NOW(), 'patate'),
       (X'c80d54d3500d453994798e8961477195', NOW(), 'oignon')
;


insert into ingredient_unit(ingredient_id, unit_id)
values (X'c80d54d3500d453994798e8961477193', X'c80d54d3500d453994798e8961477110'),
       (X'c80d54d3500d453994798e8961477193', X'c80d54d3500d453994798e8961477113'),
       (X'c80d54d3500d453994798e8961477194', X'c80d54d3500d453994798e8961477111'),
       (X'c80d54d3500d453994798e8961477195', X'c80d54d3500d453994798e8961477112')
;

insert into ingredient_types(ingredient_id, types)
values (X'c80d54d3500d453994798e8961477193', 'BEEF'),
       (X'c80d54d3500d453994798e8961477194', 'ALCOHOL')
;