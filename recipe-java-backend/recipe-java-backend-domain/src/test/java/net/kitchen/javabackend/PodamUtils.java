package net.kitchen.javabackend;

import net.kitchen.javabackend.domain.model.Measure;
import net.kitchen.javabackend.domain.model.repository.IngredientRepository;
import net.kitchen.javabackend.domain.model.repository.PictureRepository;
import net.kitchen.javabackend.dto.create.CreateUpdateMeasureDto;
import net.kitchen.javabackend.dto.create.CreateUpdateRecipeDto;
import uk.co.jemos.podam.api.AttributeMetadata;
import uk.co.jemos.podam.api.DataProviderStrategy;
import uk.co.jemos.podam.api.DefaultClassInfoStrategy;
import uk.co.jemos.podam.api.PodamFactoryImpl;
import uk.co.jemos.podam.typeManufacturers.AbstractTypeManufacturer;

import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static org.apache.commons.lang3.RandomUtils.nextInt;

public class PodamUtils {

    private static final PodamFactoryImpl podamFactory = customPodamFactory();

    private static PodamFactoryImpl customPodamFactory() {
        var factory = new PodamFactoryImpl();
        var classStrategy = DefaultClassInfoStrategy.getInstance();
        classStrategy
                .addExcludedField(Measure.class, Measure.Fields.recipe)
        ;
        factory.setClassStrategy(classStrategy);

        var strategy = factory.getStrategy();
        strategy.addOrReplaceTypeManufacturer(UUID.class, new IngredientUuidTypeManufacturer());
        return factory;
    }

    public static <T> T manufacturePojo(Class<T> pojoClass, Type... genericTypeArgs) {
        return podamFactory.manufacturePojo(pojoClass, genericTypeArgs);
    }

    public static CreateUpdateRecipeDto manufactureCreateRecipeDto(IngredientRepository ingredientRepository,
                                                                   PictureRepository pictureRepository) {
        var createRecipeDto = manufacturePojo(CreateUpdateRecipeDto.class);
        createRecipeDto.getMeasures().forEach(createMeasureDto -> {
            var units = ingredientRepository.findById(createMeasureDto.getIngredientId()).orElseThrow().getUnits();
            createMeasureDto.setUnitId(units.stream().findAny().orElseThrow().getId());
        });
        createRecipeDto.setPictureIds(List.of(pictureRepository.findAll().get(0).getId()));
        return createRecipeDto;
    }

    private static class IngredientUuidTypeManufacturer extends AbstractTypeManufacturer<UUID> {
        List<UUID> ingredientIds = List.of(
                UUID.fromString("c80d54d3-500d-4539-9479-8e8961477193"),
                UUID.fromString("c80d54d3-500d-4539-9479-8e8961477194"),
                UUID.fromString("c80d54d3-500d-4539-9479-8e8961477195")
        );

        @Override
        public UUID getType(DataProviderStrategy strategy, AttributeMetadata attributeMetadata,
                            Map<String, Type> genericTypesArgumentsMap) {

            if (attributeMetadata.getAttributeName() != null) {
                if (CreateUpdateMeasureDto.Fields.ingredientId.equals(attributeMetadata.getAttributeName())) {
                    return ingredientIds.get(nextInt(0, ingredientIds.size()));
                }
            }
            return UUID.randomUUID();
        }
    }


}
