package net.kitchen.javabackend.service;

import net.kitchen.javabackend.PodamUtils;
import net.kitchen.javabackend.RecipeIT;
import net.kitchen.javabackend.domain.model.BaseEntity;
import net.kitchen.javabackend.domain.model.Measure;
import net.kitchen.javabackend.domain.model.Recipe;
import net.kitchen.javabackend.domain.model.Unit;
import net.kitchen.javabackend.domain.model.repository.IngredientRepository;
import net.kitchen.javabackend.domain.model.repository.PictureRepository;
import net.kitchen.javabackend.domain.model.repository.RecipeRepository;
import net.kitchen.javabackend.dto.create.UpdateIngredientUnitsDto;
import org.assertj.core.groups.Tuple;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.Set;
import java.util.UUID;

import static java.time.LocalDate.now;
import static net.kitchen.javabackend.domain.model.BaseEntity.Fields.createdAt;
import static net.kitchen.javabackend.domain.model.BaseEntity.Fields.id;
import static org.assertj.core.api.Assertions.assertThat;

@RecipeIT
class RecipeServiceIT {
    @Autowired
    private RecipeRepository recipeRepository;
    @Autowired
    private IngredientRepository ingredientRepository;
    @Autowired
    private RecipeService recipeService;

    @Autowired
    private TransactionTemplate transactionTemplate;
    @Autowired
    private PictureRepository pictureRepository;

    @Test
    @Sql({"/data/unit.sql", "/data/ingredient.sql", "/data/recipe.sql", "/data/picture.sql"})
    void iCanSaveANewOrder() {
        var createRecipeDto = PodamUtils.manufactureCreateRecipeDto(ingredientRepository, pictureRepository);

        var recipeId = recipeService.createRecipe(createRecipeDto);
        assertThat(recipeId).isNotNull();
        transactionTemplate.executeWithoutResult(transactionStatus -> {
            var recipe = recipeRepository.findById(recipeId).orElseThrow();
            assertThat(recipe)
                    .satisfies(recipe1 -> assertThat(recipe.getCreatedAt()).isEqualTo(now()))
                    .usingRecursiveComparison()
                    .ignoringFields(id, createdAt, Recipe.Fields.measures, Recipe.Fields.pictures)
                    .isEqualTo(createRecipeDto);

            assertThat(recipe.getPictures())
                    .extracting(BaseEntity::getId)
                    .containsExactlyInAnyOrderElementsOf(createRecipeDto.getPictureIds());

            assertThat(recipe.getMeasures())
                    .extracting(measure -> measure.getIngredient().getId(), Measure::getQuantity,
                            measure1 -> measure1.getUnit().getId())
                    .containsExactlyInAnyOrderElementsOf(
                            createRecipeDto.getMeasures()
                                    .stream()
                                    .map(createMeasureDto -> Tuple.tuple(createMeasureDto.getIngredientId(),
                                            createMeasureDto.getQuantity(), createMeasureDto.getUnitId()))
                                    .toList()
                    );
        });
    }


    @Test
    @Sql({"/data/unit.sql", "/data/ingredient.sql"})
    void iCanUpdateUnitsForIngredient() {

        var ingredientId = UUID.fromString("c80d54d3-500d-4539-9479-8e8961477193");
        var ingredient = ingredientRepository.findById(ingredientId)
                .orElseThrow();
        assertThat(ingredient.getUnits()).extracting(Unit::getName).containsExactlyInAnyOrder("l", "ml");

        recipeService.updateIngredientUnits(ingredientId,
                new UpdateIngredientUnitsDto(Set.of(UUID.fromString("c80d54d3-500d-4539-9479-8e8961477112"))));
        ingredient = ingredientRepository.findById(ingredientId)
                .orElseThrow();
        assertThat(ingredient.getUnits()).extracting(Unit::getName).containsExactlyInAnyOrder("Unit");
    }
}
