package net.kitchen.javabackend.service.mappers;

import net.kitchen.javabackend.domain.model.*;
import net.kitchen.javabackend.dto.create.CreateUpdateRecipeDto;
import org.assertj.core.groups.Tuple;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;
import org.mockito.MockedStatic;
import org.mockito.Mockito;

import java.util.stream.Collectors;

import static net.kitchen.javabackend.PodamUtils.manufacturePojo;
import static net.kitchen.javabackend.domain.model.BaseEntity.Fields.createdAt;
import static net.kitchen.javabackend.domain.model.BaseEntity.Fields.id;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;

class RecipeMapperTest {

    private final RecipeMapper mapper = Mappers.getMapper(RecipeMapper.class);

    @Test
    void createRecipeDtoShouldMapToRecipe() {
        try (MockedStatic<RepositoryMapperUtil> utilities = Mockito.mockStatic(RepositoryMapperUtil.class)) {
            var createRecipeDto = manufacturePojo(CreateUpdateRecipeDto.class);

            utilities.when(() -> RepositoryMapperUtil.findIngredientById(any())).then(invocation -> Ingredient.builder()
                    .id(invocation.getArgument(0))
                    .units(createRecipeDto.getMeasures()
                            .stream()
                            .map(createMeasureDto1 -> Unit.builder().id(createMeasureDto1.getUnitId()).build())
                            .collect(Collectors.toSet()))
                    .build());

            utilities.when(() -> RepositoryMapperUtil.findPictureById(any()))
                    .then(invocation -> Picture.builder().id(invocation.getArgument(0)).build());

            var recipe = mapper.toRecipe(createRecipeDto);
            assertThat(recipe)
                    .usingRecursiveComparison()
                    .ignoringFields(id, createdAt, Recipe.Fields.measures, Recipe.Fields.pictures)
                    .isEqualTo(createRecipeDto);
            assertThat(recipe.getPictures())
                    .extracting(BaseEntity::getId)
                    .containsExactlyInAnyOrderElementsOf(createRecipeDto.getPictureIds());

            assertThat(recipe.getMeasures())
                    .extracting(measure -> measure.getIngredient().getId(),
                            Measure::getQuantity,
                            measure -> measure.getUnit().getId())
                    .containsExactlyInAnyOrderElementsOf(
                            createRecipeDto.getMeasures()
                                    .stream()
                                    .map(createMeasureDto -> Tuple.tuple(createMeasureDto.getIngredientId(),
                                            createMeasureDto.getQuantity(), createMeasureDto.getUnitId()))
                                    .toList()
                    );
        }
    }

    @Test
    void updateRecipeShouldReplaceEverything() {
        try (MockedStatic<RepositoryMapperUtil> utilities = Mockito.mockStatic(RepositoryMapperUtil.class)) {
            var createRecipeDto = manufacturePojo(CreateUpdateRecipeDto.class);
            var recipe = manufacturePojo(Recipe.class);

            utilities.when(() -> RepositoryMapperUtil.findIngredientById(any())).then(invocation -> Ingredient.builder()
                    .id(invocation.getArgument(0))
                    .units(createRecipeDto.getMeasures()
                            .stream()
                            .map(createMeasureDto1 -> Unit.builder().id(createMeasureDto1.getUnitId()).build())
                            .collect(Collectors.toSet()))
                    .build());

            utilities.when(() -> RepositoryMapperUtil.findPictureById(any()))
                    .then(invocation -> Picture.builder().id(invocation.getArgument(0)).build());

            mapper.updateRecipe(recipe, createRecipeDto);
            assertThat(recipe)
                    .usingRecursiveComparison()
                    .ignoringFields(id, createdAt, Recipe.Fields.measures, Recipe.Fields.pictures)
                    .isEqualTo(createRecipeDto);
            assertThat(recipe.getPictures())
                    .extracting(BaseEntity::getId)
                    .containsExactlyInAnyOrderElementsOf(createRecipeDto.getPictureIds());

            assertThat(recipe.getMeasures())
                    .extracting(measure -> measure.getIngredient().getId(),
                            Measure::getQuantity,
                            measure -> measure.getUnit().getId())
                    .containsExactlyInAnyOrderElementsOf(
                            createRecipeDto.getMeasures()
                                    .stream()
                                    .map(createMeasureDto -> Tuple.tuple(createMeasureDto.getIngredientId(),
                                            createMeasureDto.getQuantity(), createMeasureDto.getUnitId()))
                                    .toList()
                    );
        }
    }
}