package net.kitchen.javabackend.domain.model.repository;

import net.kitchen.javabackend.RecipeIT;
import net.kitchen.javabackend.domain.model.BaseEntity;
import net.kitchen.javabackend.domain.model.Ingredient;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.jdbc.Sql;

import java.util.Set;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;


@RecipeIT
class RecipeRepositoryIT {

    @Autowired
    private RecipeRepository recipeRepository;


    @Test
    @Sql({"/data/unit.sql", "/data/ingredient.sql", "/data/recipe.sql", "/data/measure.sql", "/data/picture.sql"})
    void findAll() {
        var recipes = recipeRepository.findAllByCriteria(null, null);
        assertThat(recipes).hasSize(2)
                .extracting(BaseEntity::getId)
                .containsExactlyInAnyOrder(UUID.fromString("c80d54d3-500d-4539-9479-8e8961477193"),
                        UUID.fromString("c80d54d3-500d-4539-9479-8e8961477194"));
    }

    @Test
    @Sql({"/data/unit.sql", "/data/ingredient.sql", "/data/recipe.sql", "/data/measure.sql", "/data/picture.sql"})
    void filterOnIngredientTypeContains() {
        var recipes = recipeRepository.findAllByCriteria(Set.of(Ingredient.IngredientTypeEnum.BEEF), null);
        assertThat(recipes).hasSize(1)
                .extracting(BaseEntity::getId)
                .containsExactlyInAnyOrder(UUID.fromString("c80d54d3-500d-4539-9479-8e8961477193"));
    }

    @Test
    @Sql({"/data/unit.sql", "/data/ingredient.sql", "/data/recipe.sql", "/data/measure.sql", "/data/picture.sql"})
    void filterOnIngredientTypeDoesNotContain() {
        var recipes = recipeRepository.findAllByCriteria(null, Set.of(Ingredient.IngredientTypeEnum.BEEF));
        assertThat(recipes).hasSize(1)
                .extracting(BaseEntity::getId)
                .containsExactlyInAnyOrder(UUID.fromString("c80d54d3-500d-4539-9479-8e8961477194"));
    }
}