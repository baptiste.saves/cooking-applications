package net.kitchen.javabackend.domain.exceptions;

import org.springframework.http.HttpStatus;

public class NotFoundException extends CommonException {

    public NotFoundException(ErrorCode message) {
        super(message);
        httpStatus = HttpStatus.NOT_FOUND;
    }

    public NotFoundException(ErrorCode message, Throwable cause) {
        super(message, cause);
        httpStatus = HttpStatus.NOT_FOUND;
    }
}
