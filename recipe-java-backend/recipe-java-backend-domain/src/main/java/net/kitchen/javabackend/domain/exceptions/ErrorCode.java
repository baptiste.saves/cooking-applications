package net.kitchen.javabackend.domain.exceptions;

import lombok.Getter;

import java.io.Serializable;

@Getter
public enum ErrorCode implements Serializable {

    RECIPE_NOT_FOUND(1000, "The recipe does not exist"),
    INGREDIENT_NOT_FOUND(1001, "The ingredient does not exist"),
    UNIT_NOT_FOUND(1002, "The unit does not exist"),
    PICTURE_NOT_FOUND(1003, "The picture does not exist"),
    UNIT_NOT_FOUND_FOR_INGREDIENT(2000, "The unit does not exist for this ingredient");
    private final String message;
    private final Integer code;

    ErrorCode(Integer code, String message) {
        this.message = message;
        this.code = code;
    }

}
