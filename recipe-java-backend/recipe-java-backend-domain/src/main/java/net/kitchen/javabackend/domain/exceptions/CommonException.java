package net.kitchen.javabackend.domain.exceptions;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;

@Slf4j
@Data
public abstract class CommonException extends RuntimeException {

    private final ErrorCode errorCode;
    protected HttpStatus httpStatus;

    protected CommonException(ErrorCode message) {
        super(message.getMessage());
        this.errorCode = message;
    }

    protected CommonException(ErrorCode message, Throwable cause) {
        super(message.getMessage(), cause);
        this.errorCode = message;
    }

}
