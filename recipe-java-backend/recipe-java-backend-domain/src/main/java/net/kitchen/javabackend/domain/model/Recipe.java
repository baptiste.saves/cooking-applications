package net.kitchen.javabackend.domain.model;

import lombok.*;
import lombok.experimental.FieldNameConstants;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.Type;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.List;

import static net.kitchen.javabackend.domain.model.Measure.Fields.recipe;


@Entity
@SuperBuilder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@FieldNameConstants
public class Recipe extends BaseEntity {
    private String title;

    @Type(type = "text")
    private String content;
    private int yieldQuantity;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = recipe, cascade = CascadeType.ALL, orphanRemoval = true)
    @Builder.Default
    private List<Measure> measures = new ArrayList<>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = recipe, cascade = CascadeType.ALL, orphanRemoval = true)
    @Builder.Default
    private List<Picture> pictures = new ArrayList<>();
    private String link;
}
