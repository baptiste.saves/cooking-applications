package net.kitchen.javabackend.service;

import net.kitchen.javabackend.domain.exceptions.NotFoundException;
import net.kitchen.javabackend.domain.model.Ingredient;
import net.kitchen.javabackend.domain.model.Recipe;
import net.kitchen.javabackend.domain.model.Unit;
import net.kitchen.javabackend.domain.model.repository.IngredientRepository;
import net.kitchen.javabackend.domain.model.repository.RecipeRepository;
import net.kitchen.javabackend.domain.model.repository.UnitRepository;
import net.kitchen.javabackend.dto.IngredientTypeEnum;
import net.kitchen.javabackend.dto.create.*;
import net.kitchen.javabackend.service.mappers.RecipeMapper;
import org.apache.commons.collections4.CollectionUtils;
import org.mapstruct.factory.Mappers;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import static net.kitchen.javabackend.domain.exceptions.ErrorCode.*;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

@Service
public class RecipeService {
    private final RecipeRepository recipeRepository;
    private final RecipeMapper mapper = Mappers.getMapper(RecipeMapper.class);
    private final IngredientRepository ingredientRepository;
    private final UnitRepository unitRepository;

    public RecipeService(RecipeRepository recipeRepository, IngredientRepository ingredientRepository,
                         UnitRepository unitRepository) {
        this.recipeRepository = recipeRepository;
        this.ingredientRepository = ingredientRepository;
        this.unitRepository = unitRepository;
    }

    private static Set<Ingredient.IngredientTypeEnum> convertIngredientTypeEnum(Set<IngredientTypeEnum> types) {
        if (CollectionUtils.isEmpty(types)) {
            return null;
        }
        return types.stream()
                .map(ingredientTypeEnum -> Ingredient.IngredientTypeEnum.valueOf(ingredientTypeEnum.name()))
                .collect(Collectors.toSet());
    }

    @Transactional
    @CacheEvict(value = "picture", allEntries = true, beforeInvocation = true)
    public UUID createRecipe(CreateUpdateRecipeDto createUpdateRecipeDto) {
        return recipeRepository.save(mapper.toRecipe(createUpdateRecipeDto)).getId();
    }

    @Transactional
    @CacheEvict(value = "picture", allEntries = true, beforeInvocation = true)
    public UUID updateRecipe(UUID id, CreateUpdateRecipeDto createUpdateRecipeDto) {
        var recipe = recipeRepository.findById(id).orElseThrow(() -> new NotFoundException(RECIPE_NOT_FOUND));
        return recipeRepository.save(mapper.updateRecipe(recipe, createUpdateRecipeDto)).getId();
    }

    public List<Recipe> getRecipes(Set<IngredientTypeEnum> typeContains, Set<IngredientTypeEnum> typeDoesNotContain) {
        return recipeRepository.findAllByCriteria(
                convertIngredientTypeEnum(typeContains),
                convertIngredientTypeEnum(typeDoesNotContain)
        );
    }

    public Recipe getRecipe(UUID id) {
        return recipeRepository.findById(id).orElseThrow(() -> new NotFoundException(RECIPE_NOT_FOUND));
    }

    public Ingredient getIngredient(UUID id) {
        return ingredientRepository.findById(id).orElseThrow(() -> new NotFoundException(INGREDIENT_NOT_FOUND));
    }

    public List<Ingredient> getIngredients(String name) {
        if (isNotBlank(name)) {
            return ingredientRepository.findAllNameLike(name);
        }
        return ingredientRepository.findAll();
    }

    @Transactional
    @CacheEvict(value = "ingredient", allEntries = true)
    public Ingredient createIngredient(CreateIngredientDto createIngredientDto) {
        return ingredientRepository.save(Ingredient.builder().name(createIngredientDto.getName()).build());
    }

    @Transactional
    @CacheEvict(value = "ingredient", allEntries = true)
    public Ingredient updateIngredientUnits(UUID id, UpdateIngredientUnitsDto updateIngredientUnitsDto) {
        var ingredient = ingredientRepository.findById(id)
                .orElseThrow(() -> new NotFoundException(INGREDIENT_NOT_FOUND));
        ingredient.setUnits(updateIngredientUnitsDto.getUnits()
                .stream()
                .map(uuid -> unitRepository.findById(uuid).orElseThrow(() -> new NotFoundException(UNIT_NOT_FOUND)))
                .collect(Collectors.toSet()));
        return ingredientRepository.save(ingredient);
    }

    @Transactional
    @CacheEvict(value = "ingredient", allEntries = true)
    public Ingredient updateIngredientTypes(UUID id, UpdateIngredientTypesDto updateIngredientUnitsDto) {
        var ingredient = ingredientRepository.findById(id)
                .orElseThrow(() -> new NotFoundException(INGREDIENT_NOT_FOUND));
        ingredient.setTypes(convertIngredientTypeEnum(updateIngredientUnitsDto.getTypes()));
        return ingredientRepository.save(ingredient);
    }

    @Transactional
    @CacheEvict(value = "unit", allEntries = true)
    public Unit createUnit(CreateUnitDto createUnitDto) {
        return unitRepository.save(Unit.builder().name(createUnitDto.getName()).build());
    }

    public List<Unit> getAllUnits() {
        return unitRepository.findAll();
    }


}
