package net.kitchen.javabackend.domain.model.repository;

import net.kitchen.javabackend.domain.model.Unit;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface UnitRepository extends JpaRepository<Unit, UUID> {

    @Cacheable("unit")
    @Override
    Optional<Unit> findById(UUID id);

    @Override
    @Cacheable("unit")
    List<Unit> findAll();

}
