package net.kitchen.javabackend.service.mappers;

import net.kitchen.javabackend.domain.exceptions.BadRequestException;
import net.kitchen.javabackend.domain.exceptions.ErrorCode;
import net.kitchen.javabackend.domain.model.Measure;
import net.kitchen.javabackend.domain.model.Recipe;
import net.kitchen.javabackend.domain.model.Unit;
import net.kitchen.javabackend.dto.create.CreateUpdateMeasureDto;
import net.kitchen.javabackend.dto.create.CreateUpdateRecipeDto;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

@Mapper(uses = {RecipeMapper.MeasureMapper.class, RepositoryMapperUtil.class}, builder = @org.mapstruct.Builder(disableBuilder = true))
public interface RecipeMapper {

    @Mapping(source = "pictureIds", target = "pictures")
    Recipe toRecipe(CreateUpdateRecipeDto createUpdateRecipeDto);

    @Mapping(source = "pictureIds", target = "pictures")
    Recipe updateRecipe(@MappingTarget Recipe recipe, CreateUpdateRecipeDto createUpdateRecipeDto);

    @AfterMapping
    default void setRecipeToSublists(@MappingTarget Recipe recipe) {
        recipe.getMeasures().forEach(measure -> measure.setRecipe(recipe));
        recipe.getPictures().forEach(picture -> picture.setRecipe(recipe));
    }


    @Mapper(uses = RepositoryMapperUtil.class)
    interface MeasureMapper {
        @Mapping(source = "ingredientId", target = "ingredient")
        @Mapping(source = ".", target = "unit")
        Measure toMeasure(CreateUpdateMeasureDto createUpdateMeasureDto);

        default Unit createMeasureDtoToUnit(CreateUpdateMeasureDto createUpdateMeasureDto) {
            var units = RepositoryMapperUtil.findIngredientById(createUpdateMeasureDto.getIngredientId()).getUnits();
            return units.stream()
                    .filter(unit -> unit.getId().equals(createUpdateMeasureDto.getUnitId()))
                    .findFirst()
                    .orElseThrow(() -> new BadRequestException(ErrorCode.UNIT_NOT_FOUND_FOR_INGREDIENT));
        }
    }


}
