package net.kitchen.javabackend.domain.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import static javax.persistence.FetchType.EAGER;

@Entity
@SuperBuilder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@FieldNameConstants
public class Measure extends BaseEntity {
    @ManyToOne
    private Recipe recipe;
    @ManyToOne(fetch = EAGER)
    @Fetch(FetchMode.JOIN)
    private Ingredient ingredient;
    @ManyToOne(fetch = EAGER)
    @Fetch(FetchMode.JOIN)
    private Unit unit;
    private Double quantity;
}
