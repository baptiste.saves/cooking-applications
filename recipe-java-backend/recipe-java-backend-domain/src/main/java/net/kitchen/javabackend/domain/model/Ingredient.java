package net.kitchen.javabackend.domain.model;

import lombok.*;
import lombok.experimental.FieldNameConstants;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

import static javax.persistence.FetchType.EAGER;

@Entity
@SuperBuilder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@FieldNameConstants
public class Ingredient extends BaseEntity {

    private String name;

    @ManyToMany(fetch = EAGER)
    @Fetch(FetchMode.JOIN)
    @JoinTable(name = "ingredient_unit",
            joinColumns = {@JoinColumn(name = "ingredient_id")},
            inverseJoinColumns = {@JoinColumn(name = "unit_id")}
    )
    @Builder.Default
    private Set<Unit> units = new HashSet<>();

    @Enumerated(EnumType.STRING)
    @ElementCollection(targetClass = IngredientTypeEnum.class, fetch = EAGER)
    @Fetch(FetchMode.JOIN)
    @Builder.Default
    private Set<IngredientTypeEnum> types = new HashSet<>();

    public enum IngredientTypeEnum {
        VEGETABLE, FRUIT, BEEF, DUCK, CHICKEN, FISH, ALCOHOL, PASTA, RICE, PORK, DAIRY, VEAL
    }
}
