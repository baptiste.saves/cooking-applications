package net.kitchen.javabackend.domain.model.repository;

import net.kitchen.javabackend.domain.model.Picture;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface PictureRepository extends JpaRepository<Picture, UUID> {

    @Override
    @Cacheable("picture")
    Optional<Picture> findById(UUID id);
}
