package net.kitchen.javabackend.domain.exceptions;

import org.springframework.http.HttpStatus;

public class InternalErrorException extends CommonException {
    public InternalErrorException(ErrorCode message) {
        super(message);
        httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
    }

    public InternalErrorException(ErrorCode message, Throwable cause) {
        super(message, cause);
        httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
    }

}
