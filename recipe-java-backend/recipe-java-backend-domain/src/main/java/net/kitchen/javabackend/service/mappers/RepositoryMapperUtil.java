package net.kitchen.javabackend.service.mappers;


import net.kitchen.javabackend.domain.exceptions.NotFoundException;
import net.kitchen.javabackend.domain.model.Ingredient;
import net.kitchen.javabackend.domain.model.Picture;
import net.kitchen.javabackend.domain.model.repository.IngredientRepository;
import net.kitchen.javabackend.domain.model.repository.PictureRepository;
import org.springframework.stereotype.Component;

import java.util.UUID;

import static net.kitchen.javabackend.domain.exceptions.ErrorCode.INGREDIENT_NOT_FOUND;
import static net.kitchen.javabackend.domain.exceptions.ErrorCode.PICTURE_NOT_FOUND;

@Component
public class RepositoryMapperUtil {
    private static PictureRepository pictureRepository;
    private static IngredientRepository ingredientRepository;

    public RepositoryMapperUtil(IngredientRepository ingredientRepository, PictureRepository pictureRepository) {
        RepositoryMapperUtil.ingredientRepository = ingredientRepository;
        RepositoryMapperUtil.pictureRepository = pictureRepository;
    }

    public static Ingredient findIngredientById(UUID ingredientId) {
        return ingredientRepository.findById(ingredientId)
                .orElseThrow(() -> new NotFoundException(INGREDIENT_NOT_FOUND));
    }

    public static Picture findPictureById(UUID pictureId) {
        return pictureRepository.findById(pictureId)
                .orElseThrow(() -> new NotFoundException(PICTURE_NOT_FOUND));
    }

}