package net.kitchen.javabackend.domain.model.repository;

import net.kitchen.javabackend.domain.model.*;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;

@Repository
public interface RecipeRepository extends JpaRepository<Recipe, UUID>, JpaSpecificationExecutor<Recipe> {

    default List<Recipe> findAllByCriteria(Set<Ingredient.IngredientTypeEnum> typeContains,
                                           Set<Ingredient.IngredientTypeEnum> typeDoesNotContains) {
        return this.findAll((Root<Recipe> root, CriteriaQuery<?> cq, CriteriaBuilder cb) -> {
            var predicates = new ArrayList<Predicate>();
            cq.distinct(true);
            if (CollectionUtils.isNotEmpty(typeContains)) {
                predicates.add(root.join(Recipe_.MEASURES)
                        .join(Measure_.INGREDIENT)
                        .join(Ingredient_.TYPES)
                        .in(typeContains));
            }
            if (CollectionUtils.isNotEmpty(typeDoesNotContains)) {
                var subquery = cq.subquery(Recipe.class);
                Root<Recipe> subqueryRoot = subquery.from(Recipe.class);
                subquery.select(subqueryRoot);
                subquery.where(
                        cb.and(subqueryRoot.join(Recipe_.MEASURES)
                                        .join(Measure_.INGREDIENT)
                                        .join(Ingredient_.TYPES)
                                        .in(typeDoesNotContains),
                                cb.equal(root, subqueryRoot)
                        )
                );
                predicates.add(cb.not(cb.exists(subquery)));
            }
            return cb.and(predicates.toArray(new Predicate[0]));
        }, Sort.by(Sort.Order.asc(Recipe.Fields.title)));
    }
}
