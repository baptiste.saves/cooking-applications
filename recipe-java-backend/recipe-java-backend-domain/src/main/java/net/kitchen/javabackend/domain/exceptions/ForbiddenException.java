package net.kitchen.javabackend.domain.exceptions;

import org.springframework.http.HttpStatus;

public class ForbiddenException extends CommonException {

    public ForbiddenException(ErrorCode message) {
        super(message);
        httpStatus = HttpStatus.FORBIDDEN;
    }

    public ForbiddenException(ErrorCode message, Throwable cause) {
        super(message, cause);
        httpStatus = HttpStatus.FORBIDDEN;
    }
}