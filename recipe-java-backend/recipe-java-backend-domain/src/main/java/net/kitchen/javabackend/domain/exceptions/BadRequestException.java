package net.kitchen.javabackend.domain.exceptions;

import org.springframework.http.HttpStatus;

public class BadRequestException extends CommonException {

    public BadRequestException(ErrorCode message) {
        super(message);
        httpStatus = HttpStatus.BAD_REQUEST;
    }

    public BadRequestException(ErrorCode message, Throwable cause) {
        super(message, cause);
        httpStatus = HttpStatus.BAD_REQUEST;
    }
}