package net.kitchen.javabackend.service;

import net.kitchen.javabackend.domain.exceptions.NotFoundException;
import net.kitchen.javabackend.domain.model.Picture;
import net.kitchen.javabackend.domain.model.repository.PictureRepository;
import org.springframework.stereotype.Service;

import java.util.UUID;

import static net.kitchen.javabackend.domain.exceptions.ErrorCode.PICTURE_NOT_FOUND;

@Service
public class PictureService {

    private final PictureRepository pictureRepository;

    public PictureService(PictureRepository pictureRepository) {
        this.pictureRepository = pictureRepository;
    }

    public UUID uploadPicture(byte[] bytes) {
        return pictureRepository.save(Picture.builder().content(bytes).build()).getId();
    }

    public byte[] getPicture(UUID id) {
        return pictureRepository.findById(id).orElseThrow(() -> new NotFoundException(PICTURE_NOT_FOUND)).getContent();
    }
}
