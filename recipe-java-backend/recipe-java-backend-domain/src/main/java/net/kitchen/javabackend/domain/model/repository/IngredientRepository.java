package net.kitchen.javabackend.domain.model.repository;

import net.kitchen.javabackend.domain.model.Ingredient;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface IngredientRepository extends JpaRepository<Ingredient, UUID> {

    @Query(value = """
            select ing from Ingredient ing where
               :name like concat( '%', ing.name,'%')
               or ing.name like  concat( '%', :name,'%')
               """
    )
    List<Ingredient> findAllNameLike(@Param("name") String name);

    @Cacheable("ingredient")
    @Override
    Optional<Ingredient> findById(UUID id);

}
