create table ingredient_types
(
    ingredient_id binary(16)   not null,
    types         varchar(100) not null,
    PRIMARY KEY (ingredient_id, types)
)