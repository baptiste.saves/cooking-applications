create table recipe
(
    id             binary(16)   not null primary key,
    created_at     date         not null,
    title          varchar(200) not null,
    yield_quantity INTEGER      not null,
    content        text         not null
);
create table picture
(
    id         binary(16) not null primary key,
    created_at date       not null,
    content    longblob   not null,
    recipe_id  binary(16),
    constraint picture_recipe foreign key (recipe_id) references recipe (id)
);
create table unit
(
    id         binary(16)   not null primary key,
    created_at date         not null,
    name       varchar(255) not null,
    unique (name)
);

create table ingredient
(
    id         binary(16)   not null primary key,
    created_at date         not null,
    name       varchar(255) not null,
    unique (name)
);

create table ingredient_unit
(
    ingredient_id binary(16) not null,
    unit_id       binary(16) not null,
    PRIMARY KEY (ingredient_id, unit_id),
    constraint ingredient_unit_ingredient foreign key (ingredient_id) references ingredient (id),
    constraint ingredient_unit_unit foreign key (unit_id) references unit (id)
);

create table measure
(
    id            binary(16) not null primary key,
    created_at    date       not null,
    quantity      double     not null,
    ingredient_id binary(16) not null,
    unit_id       binary(16) not null,
    recipe_id     binary(16) not null,
    constraint measure_ingredient foreign key (ingredient_id) references ingredient (id),
    constraint measure_recipe foreign key (recipe_id) references recipe (id),
    constraint measure_unit foreign key (unit_id) references unit (id)
);
