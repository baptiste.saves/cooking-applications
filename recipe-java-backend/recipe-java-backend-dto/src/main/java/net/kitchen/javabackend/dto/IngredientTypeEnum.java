package net.kitchen.javabackend.dto;

import io.swagger.v3.oas.annotations.media.Schema;

@Schema(enumAsRef = true)
public enum IngredientTypeEnum {
    VEGETABLE, FRUIT, BEEF, DUCK, CHICKEN, FISH, ALCOHOL, PASTA, RICE, PORK, DAIRY, VEAL
}
