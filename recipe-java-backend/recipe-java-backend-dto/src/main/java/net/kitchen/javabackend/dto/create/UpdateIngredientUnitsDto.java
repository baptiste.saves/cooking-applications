package net.kitchen.javabackend.dto.create;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldNameConstants;

import javax.validation.constraints.NotEmpty;
import java.util.Set;
import java.util.UUID;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
@FieldNameConstants
public class UpdateIngredientUnitsDto {
    @NotEmpty
    private Set<UUID> units;
}
