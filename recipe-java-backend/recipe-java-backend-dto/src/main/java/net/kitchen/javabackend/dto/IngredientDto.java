package net.kitchen.javabackend.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldNameConstants;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Set;
import java.util.UUID;


@NoArgsConstructor
@AllArgsConstructor
@Builder
@FieldNameConstants
@Data
public class IngredientDto {
    @NotEmpty
    private UUID id;
    @NotEmpty
    private String name;
    @NotNull
    private Set<UnitDto> units;
    @NotNull
    private Set<IngredientTypeEnum> types;
}
