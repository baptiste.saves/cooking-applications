package net.kitchen.javabackend.dto;

import lombok.*;
import lombok.experimental.FieldNameConstants;

import java.time.LocalDate;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@FieldNameConstants
@ToString
public class RecipeListDto {
    private UUID id;
    private LocalDate createdAt;
    private String title;
    private UUID picture;
}
