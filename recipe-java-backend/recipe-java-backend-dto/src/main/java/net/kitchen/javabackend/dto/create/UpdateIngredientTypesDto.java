package net.kitchen.javabackend.dto.create;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldNameConstants;
import net.kitchen.javabackend.dto.IngredientTypeEnum;

import javax.validation.constraints.NotEmpty;
import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
@FieldNameConstants
public class UpdateIngredientTypesDto {
    @NotEmpty
    private Set<IngredientTypeEnum> types;

}
