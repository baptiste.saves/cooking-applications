package net.kitchen.javabackend.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldNameConstants;

import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@FieldNameConstants
@Data
public class IngredientTypeDto {
    private Set<IngredientTypeEnum> types;
}
