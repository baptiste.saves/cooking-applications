package net.kitchen.javabackend.dto.create;

import lombok.*;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class CreateUpdateRecipeDto {

    @Size(min = 6, max = 200)
    @NotNull
    private String title;

    @Size(min = 10, max = 3000)
    @NotNull
    private String content;

    @NotNull
    @Min(1)
    private int yieldQuantity;

    @NotEmpty
    @Valid
    private List<CreateUpdateMeasureDto> measures;

    private List<UUID> pictureIds;

    private String link;
}
