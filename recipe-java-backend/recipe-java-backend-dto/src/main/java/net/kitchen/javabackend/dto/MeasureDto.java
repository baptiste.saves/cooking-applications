package net.kitchen.javabackend.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldNameConstants;

import javax.validation.constraints.NotNull;
import java.util.UUID;


@NoArgsConstructor
@AllArgsConstructor
@Builder
@FieldNameConstants
@Data
public class MeasureDto {
    @NotNull
    private double quantity;
    @NotNull
    private String ingredient;
    @NotNull
    private String unit;
    @NotNull
    private UUID ingredientId;
    @NotNull
    private UUID unitId;
}
