package net.kitchen.javabackend.dto;

import lombok.*;
import lombok.experimental.FieldNameConstants;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@FieldNameConstants
@ToString
public class RecipeDto {
    @NotNull
    private UUID id;
    @NotNull
    private LocalDate createdAt;
    @NotEmpty
    private String title;
    @NotEmpty
    private String content;
    @NotNull
    private int yieldQuantity;
    @NotEmpty
    @Valid
    private List<MeasureDto> measures;
    private List<UUID> pictureIds;
    private String link;
}
