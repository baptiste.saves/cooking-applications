package net.kitchen.javabackend.dto.create;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldNameConstants;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import java.util.UUID;


@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
@FieldNameConstants
public class CreateUpdateMeasureDto {

    @NotNull
    public UUID ingredientId;
    @NotNull
    public UUID unitId;
    @NotNull
    @Digits(integer = 10, fraction = 3)
    private double quantity;
}
