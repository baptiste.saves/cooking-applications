package net.kitchen.javabackend.controller;

import net.kitchen.javabackend.dto.IngredientTypeEnum;
import net.kitchen.javabackend.dto.RecipeDto;
import net.kitchen.javabackend.dto.RecipeListDto;
import net.kitchen.javabackend.dto.create.CreateUpdateRecipeDto;
import net.kitchen.javabackend.service.RecipeService;
import org.mapstruct.factory.Mappers;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;

@RestController
@RequestMapping(value = "/recipe", produces = "application/json")
public class RecipeController {

    private final RecipeControllerMapper recipeMapper = Mappers.getMapper(RecipeControllerMapper.class);
    private final RecipeService recipeService;

    public RecipeController(RecipeService recipeService) {
        this.recipeService = recipeService;
    }

    @PostMapping
    @ResponseStatus(CREATED)
    public UUID createRecipe(@Valid @RequestBody CreateUpdateRecipeDto createUpdateRecipeDto) {
        return recipeService.createRecipe(createUpdateRecipeDto);
    }

    @PutMapping("/{id}")
    @ResponseStatus(OK)
    public UUID updateRecipe(@PathVariable UUID id, @Valid @RequestBody CreateUpdateRecipeDto createUpdateRecipeDto) {
        return recipeService.updateRecipe(id, createUpdateRecipeDto);
    }


    @GetMapping("/{id}")
    @ResponseStatus(OK)
    public RecipeDto getRecipe(@PathVariable UUID id) {
        return recipeMapper.toRecipeDto(recipeService.getRecipe(id));
    }

    @GetMapping
    @ResponseStatus(OK)
    public List<RecipeListDto> getRecipes(@RequestParam(required = false) Set<IngredientTypeEnum> typeContains,
                                          @RequestParam(required = false) Set<IngredientTypeEnum> typeDoesNotContain) {
        //TODO split method in two so that get recipe list does not join the whole db (currently eager)
        return recipeService.getRecipes(typeContains, typeDoesNotContain)
                .stream()
                .map(recipeMapper::toRecipeListDto)
                .toList();
    }

}
