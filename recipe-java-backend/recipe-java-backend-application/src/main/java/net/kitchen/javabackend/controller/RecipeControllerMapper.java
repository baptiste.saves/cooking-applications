package net.kitchen.javabackend.controller;

import net.kitchen.javabackend.domain.model.Measure;
import net.kitchen.javabackend.domain.model.Picture;
import net.kitchen.javabackend.domain.model.Recipe;
import net.kitchen.javabackend.dto.MeasureDto;
import net.kitchen.javabackend.dto.RecipeDto;
import net.kitchen.javabackend.dto.RecipeListDto;
import org.apache.commons.collections4.CollectionUtils;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.UUID;

@Mapper(uses = RecipeControllerMapper.MeasureControllerMapper.class)
public interface RecipeControllerMapper {

    @Mapping(source = "pictures", target = "pictureIds")
    RecipeDto toRecipeDto(Recipe recipes);

    @Mapping(target = "picture", source = "recipes.pictures")
    RecipeListDto toRecipeListDto(Recipe recipes);

    default java.util.UUID map(java.util.List<net.kitchen.javabackend.domain.model.Picture> pictures) {
        if (CollectionUtils.isEmpty(pictures)) {
            return null;
        }
        return pictures.get(0).getId();
    }

    default UUID map(Picture picture) {
        if (picture == null) {
            return null;
        }
        return picture.getId();
    }

    @Mapper
    interface MeasureControllerMapper {
        @Mapping(target = "ingredient", source = "ingredient.name")
        @Mapping(target = "ingredientId", source = "ingredient.id")
        @Mapping(target = "unitId", source = "unit.id")
        @Mapping(target = "unit", source = "unit.name")
        MeasureDto toMeasureDto(Measure measure);
    }
}
