package net.kitchen.javabackend.controller;

import net.kitchen.javabackend.domain.model.Ingredient;
import net.kitchen.javabackend.domain.model.Unit;
import net.kitchen.javabackend.dto.IngredientDto;
import net.kitchen.javabackend.dto.UnitDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper
public interface IngredientControllerMapper {
    @Mapping(target = "name", source = "ingredient.name")
    @Mapping(target = "units", source = "ingredient.units")
    IngredientDto toIngredientDto(Ingredient ingredient);

    UnitDto toUnitDto(Unit unit);
}
