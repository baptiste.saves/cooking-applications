package net.kitchen.javabackend;

import lombok.extern.slf4j.Slf4j;
import net.kitchen.javabackend.domain.exceptions.CommonException;
import net.kitchen.javabackend.domain.exceptions.ErrorCode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletRequest;

@Slf4j
@ControllerAdvice(basePackages = {"net.cuisine", "net.cuisine"})
public class ErrorController {
    @ExceptionHandler(CommonException.class)
    public ResponseEntity<ErrorCode> exceptionHandler(HttpServletRequest req, CommonException e) {
        return new ResponseEntity<>(e.getErrorCode(), e.getHttpStatus());
    }

}
