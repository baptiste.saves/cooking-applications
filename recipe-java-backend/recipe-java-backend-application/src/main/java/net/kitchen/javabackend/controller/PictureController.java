package net.kitchen.javabackend.controller;

import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import net.kitchen.javabackend.service.PictureService;
import org.springframework.http.ContentDisposition;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.UUID;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.MediaType.*;

@RestController
@RequestMapping(value = "/picture", produces = APPLICATION_JSON_VALUE)
public class PictureController {

    private final PictureService pictureService;

    public PictureController(PictureService pictureService) {
        this.pictureService = pictureService;
    }

    @PostMapping(consumes = MULTIPART_FORM_DATA_VALUE)
    @ResponseStatus(CREATED)
    public UUID uploadPicture(@RequestParam MultipartFile picture) throws IOException {
        return pictureService.uploadPicture(picture.getBytes());
    }

    @GetMapping(value = "/{id}", produces = APPLICATION_OCTET_STREAM_VALUE)
    @ApiResponse(content = @Content(schema = @Schema(type = "string", format = "byte")))
    public ResponseEntity<byte[]> getPicture(@PathVariable UUID id) {

        return ResponseEntity.ok()
                .contentType(MediaType.APPLICATION_OCTET_STREAM)
                .header(HttpHeaders.CONTENT_DISPOSITION,
                        ContentDisposition.attachment()
                                .filename("whatever")
                                .build().toString())
                .body(pictureService.getPicture(id));
    }
}
