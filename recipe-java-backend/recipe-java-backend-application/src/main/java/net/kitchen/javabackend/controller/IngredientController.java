package net.kitchen.javabackend.controller;

import net.kitchen.javabackend.dto.IngredientDto;
import net.kitchen.javabackend.dto.IngredientTypeDto;
import net.kitchen.javabackend.dto.IngredientTypeEnum;
import net.kitchen.javabackend.dto.UnitDto;
import net.kitchen.javabackend.dto.create.CreateIngredientDto;
import net.kitchen.javabackend.dto.create.CreateUnitDto;
import net.kitchen.javabackend.dto.create.UpdateIngredientTypesDto;
import net.kitchen.javabackend.dto.create.UpdateIngredientUnitsDto;
import net.kitchen.javabackend.service.RecipeService;
import org.mapstruct.factory.Mappers;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;

@RestController
@RequestMapping(value = "/ingredient", produces = "application/json")
public class IngredientController {

    private final RecipeService recipeService;
    private final IngredientControllerMapper mapper = Mappers.getMapper(IngredientControllerMapper.class);

    public IngredientController(RecipeService recipeService) {
        this.recipeService = recipeService;
    }


    @PutMapping("/{id}/type")
    public IngredientDto replaceIngredientTypes(@PathVariable UUID id,
                                                @RequestBody @Valid UpdateIngredientTypesDto updateIngredientUnitsDto) {
        return mapper.toIngredientDto(recipeService.updateIngredientTypes(id, updateIngredientUnitsDto));
    }

    @PostMapping
    @ResponseStatus(CREATED)
    public IngredientDto createIngredient(@RequestBody @Valid CreateIngredientDto createIngredientDto) {
        return mapper.toIngredientDto(recipeService.createIngredient(createIngredientDto));
    }

    @PostMapping("/unit")
    @ResponseStatus(CREATED)
    public UnitDto createUnit(@RequestBody @Valid CreateUnitDto createUnitDto) {
        return mapper.toUnitDto(recipeService.createUnit(createUnitDto));
    }

    @GetMapping("/unit")
    public List<UnitDto> getAllUnits() {
        return recipeService.getAllUnits().stream().map(mapper::toUnitDto).toList();
    }

    @GetMapping("/type")
    public IngredientTypeDto getAllIngredientTypes() {
        return new IngredientTypeDto(Set.of(IngredientTypeEnum.values()));
    }

    @PutMapping("/{id}/units")
    public IngredientDto replaceIngredientUnits(@PathVariable UUID id,
                                                @RequestBody @Valid UpdateIngredientUnitsDto updateIngredientUnitsDto) {
        return mapper.toIngredientDto(recipeService.updateIngredientUnits(id, updateIngredientUnitsDto));
    }

    @GetMapping("/{id}")
    @ResponseStatus(OK)
    @Transactional
    public IngredientDto getIngredient(@PathVariable UUID id) {
        return mapper.toIngredientDto(recipeService.getIngredient(id));
    }

    @GetMapping
    @ResponseStatus(OK)
    public List<IngredientDto> getIngredients(@RequestParam(required = false) String name) {
        return recipeService.getIngredients(name).stream().map(mapper::toIngredientDto).toList();
    }
}
