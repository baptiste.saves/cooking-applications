package net.kitchen.javabackend.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import net.kitchen.javabackend.RecipeAppIT;
import net.kitchen.javabackend.domain.model.repository.IngredientRepository;
import net.kitchen.javabackend.domain.model.repository.PictureRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.io.UnsupportedEncodingException;
import java.util.UUID;

import static net.kitchen.javabackend.PodamUtils.manufactureCreateRecipeDto;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RecipeAppIT
class UpdateRecipeIT {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private IngredientRepository ingredientRepository;
    @Autowired
    private PictureRepository pictureRepository;

    @Test
    @Sql({"/data/unit.sql", "/data/ingredient.sql", "/data/recipe.sql", "/data/picture.sql"})
    @SneakyThrows
    void updateRecipeNominalCase() {
        ObjectMapper objectMapper = new ObjectMapper();
        var mvcResult = mockMvc.perform(put("/recipe/c80d54d3-500d-4539-9479-8e8961477193")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(manufactureCreateRecipeDto(ingredientRepository,
                                pictureRepository)))
                )
                .andExpect(status().isOk())
                .andReturn();
        String response = removeQuote(mvcResult);
        assertThat(UUID.fromString(response)).isInstanceOf(UUID.class);
    }

    private String removeQuote(MvcResult mvcResult) throws UnsupportedEncodingException {
        var substring = mvcResult.getResponse().getContentAsString().substring(1);
        return substring.substring(0, substring.length() - 1);
    }

}
