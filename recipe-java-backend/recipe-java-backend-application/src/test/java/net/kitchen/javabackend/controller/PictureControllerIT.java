package net.kitchen.javabackend.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import net.kitchen.javabackend.RecipeAppIT;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.io.UnsupportedEncodingException;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RecipeAppIT
class PictureControllerIT {

    @Autowired
    private MockMvc mockMvc;

    @Test
    @Sql({"/data/unit.sql", "/data/ingredient.sql"})
    @SneakyThrows
    void createGetPictureNominalCase() {
        ObjectMapper objectMapper = new ObjectMapper();
        var bytesIn = "Hello World!".getBytes();
        MockMultipartFile mockMultipartFile = new MockMultipartFile(
                "multipartFile",
                "test.xls",
                "application/x-xls",
                bytesIn);

        var mvcResult = mockMvc.perform(
                        MockMvcRequestBuilders.multipart("/picture")
                                .file("picture", mockMultipartFile.getBytes())
                )
                .andExpect(status().isCreated())
                .andReturn();
        String response = removeQuote(mvcResult);
        var id = UUID.fromString(response);
        assertThat(id).isInstanceOf(UUID.class);


        var mvcResult1 = mockMvc.perform(get("/picture/{id}", id).contentType(MediaType.APPLICATION_JSON)).andReturn();
        var bytesOut = mvcResult1.getResponse().getContentAsByteArray();

        assertThat(bytesOut).isEqualTo(bytesIn);
    }

    private String removeQuote(MvcResult mvcResult) throws UnsupportedEncodingException {
        var substring = mvcResult.getResponse().getContentAsString().substring(1);
        return substring.substring(0, substring.length() - 1);
    }


}