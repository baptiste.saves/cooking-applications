package net.kitchen.javabackend.controller;

import net.kitchen.javabackend.domain.model.BaseEntity;
import net.kitchen.javabackend.domain.model.Recipe;
import net.kitchen.javabackend.dto.MeasureDto;
import org.assertj.core.groups.Tuple;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;

import static net.kitchen.javabackend.PodamUtils.manufacturePojo;
import static net.kitchen.javabackend.dto.RecipeDto.Fields.measures;
import static net.kitchen.javabackend.dto.RecipeDto.Fields.pictureIds;
import static org.assertj.core.api.Assertions.assertThat;

class RecipeControllerMapperTest {

    private final RecipeControllerMapper mapper = Mappers.getMapper(RecipeControllerMapper.class);

    @Test
    void mapRecipeToRecipeDto() {
        var recipe = manufacturePojo(Recipe.class);

        var recipeDto = mapper.toRecipeDto(recipe);
        assertThat(recipeDto)
                .usingRecursiveComparison()
                .ignoringFields(measures, pictureIds)
                .isEqualTo(recipe);

        assertThat(recipeDto.getPictureIds())
                .containsExactlyInAnyOrderElementsOf(recipe.getPictures().stream().map(BaseEntity::getId).toList());

        assertThat(recipeDto.getMeasures()).hasSize(recipe.getMeasures().size())
                .extracting(MeasureDto::getIngredient, MeasureDto::getUnit, MeasureDto::getQuantity, MeasureDto::getIngredientId, MeasureDto::getUnitId)
                .containsExactlyInAnyOrderElementsOf(recipe.getMeasures()
                        .stream()
                        .map(measure -> Tuple.tuple(measure.getIngredient().getName(),
                                measure.getUnit().getName(), measure.getQuantity(), measure.getIngredient().getId(), measure.getUnit().getId())).toList()
                )
        ;
    }

}