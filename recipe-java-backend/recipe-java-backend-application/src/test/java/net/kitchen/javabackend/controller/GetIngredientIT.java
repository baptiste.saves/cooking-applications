package net.kitchen.javabackend.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import net.kitchen.javabackend.RecipeAppIT;
import net.kitchen.javabackend.dto.IngredientDto;
import net.kitchen.javabackend.dto.UnitDto;
import org.assertj.core.groups.Tuple;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;
import java.util.Set;
import java.util.UUID;

import static net.kitchen.javabackend.dto.IngredientTypeEnum.BEEF;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

@RecipeAppIT
class GetIngredientIT {

    private final ObjectMapper objectMapper = new ObjectMapper().findAndRegisterModules();
    @Autowired
    private MockMvc mockMvc;

    @Test
    @SneakyThrows
    @Sql(scripts = {"/data/unit.sql", "/data/ingredient.sql"})
    void getIngredient() {
        var perform = mockMvc.perform(get("/ingredient/{id}", "c80d54d3-500d-4539-9479-8e8961477193")
                .contentType(MediaType.APPLICATION_JSON)).andReturn();
        var actual = objectMapper.readValue(perform.getResponse().getContentAsString(), IngredientDto.class);

        assertThat(actual)
                .usingRecursiveComparison()
                .ignoringFields(IngredientDto.Fields.units)
                .isEqualTo(new IngredientDto(UUID.fromString("c80d54d3-500d-4539-9479-8e8961477193"),
                        "creme",
                        Set.of(UnitDto.builder()
                                        .id(UUID.fromString("c80d54d3-500d-4539-9479-8e8961477113"))
                                        .name("ml")
                                        .build(),
                                UnitDto.builder()
                                        .id(UUID.fromString("c80d54d3-500d-4539-9479-8e8961477110"))
                                        .name("l")
                                        .build()),
                        Set.of(BEEF)
                ));
    }


    @Test
    @SneakyThrows
    @Sql(scripts = {"/data/unit.sql", "/data/ingredient.sql"})
    void getIngredientList() {
        var perform = mockMvc.perform(get("/ingredient/")
                .contentType(MediaType.APPLICATION_JSON)).andReturn();
        var actual = objectMapper.readValue(perform.getResponse().getContentAsString(),
                new TypeReference<List<IngredientDto>>() {
                });

        assertThat(actual)
                .extracting(IngredientDto::getId, IngredientDto::getName, IngredientDto::getUnits)
                .containsExactlyInAnyOrder(
                        Tuple.tuple(UUID.fromString("c80d54d3-500d-4539-9479-8e8961477193"),
                                "creme",
                                Set.of(
                                        UnitDto.builder()
                                                .id(UUID.fromString("c80d54d3-500d-4539-9479-8e8961477110"))
                                                .name("l")
                                                .build(),
                                        UnitDto.builder()
                                                .id(UUID.fromString("c80d54d3-500d-4539-9479-8e8961477113"))
                                                .name("ml")
                                                .build()
                                )
                        ),
                        Tuple.tuple(UUID.fromString("c80d54d3-500d-4539-9479-8e8961477194"),
                                "patate",
                                Set.of(UnitDto.builder()
                                        .id(UUID.fromString("c80d54d3-500d-4539-9479-8e8961477111"))
                                        .name("g")
                                        .build()
                                )),
                        Tuple.tuple(UUID.fromString("c80d54d3-500d-4539-9479-8e8961477195"),
                                "oignon",
                                Set.of(UnitDto.builder()
                                        .id(UUID.fromString("c80d54d3-500d-4539-9479-8e8961477112"))
                                        .name("Unit")
                                        .build()
                                ))
                )
        ;
    }

    @Test
    @SneakyThrows
    @Sql(scripts = {"/data/unit.sql"})
    void getUnitList() {
        var perform = mockMvc.perform(get("/ingredient/unit")
                .contentType(MediaType.APPLICATION_JSON)).andReturn();
        var actual = objectMapper.readValue(perform.getResponse().getContentAsString(),
                new TypeReference<List<UnitDto>>() {
                });

        assertThat(actual)
                .extracting(UnitDto::getName)
                .containsExactlyInAnyOrder("g", "Unit", "l", "ml")
        ;
    }


    @Test
    @SneakyThrows
    @Sql(scripts = {"/data/unit.sql", "/data/ingredient.sql"})
    void getIngredientListWithName() {
        var perform = mockMvc.perform(get("/ingredient/?name=patat")
                .contentType(MediaType.APPLICATION_JSON)).andReturn();
        var actual = objectMapper.readValue(perform.getResponse().getContentAsString(),
                new TypeReference<List<IngredientDto>>() {
                });

        assertThat(actual)
                .extracting(IngredientDto::getId, IngredientDto::getName, IngredientDto::getUnits)
                .containsExactlyInAnyOrder(
                        Tuple.tuple(UUID.fromString("c80d54d3-500d-4539-9479-8e8961477194"),
                                "patate",
                                Set.of(UnitDto.builder()
                                        .id(UUID.fromString("c80d54d3-500d-4539-9479-8e8961477111"))
                                        .name("g")
                                        .build()
                                ))
                );
    }
}