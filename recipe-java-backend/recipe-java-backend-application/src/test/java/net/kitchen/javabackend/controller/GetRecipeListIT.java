package net.kitchen.javabackend.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import net.kitchen.javabackend.RecipeAppIT;
import net.kitchen.javabackend.domain.model.repository.RecipeRepository;
import net.kitchen.javabackend.dto.RecipeListDto;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

import static net.kitchen.javabackend.dto.IngredientTypeEnum.BEEF;
import static net.kitchen.javabackend.dto.IngredientTypeEnum.FISH;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

@RecipeAppIT
class GetRecipeListIT {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private RecipeRepository recipeRepository;

    @Test
    @SneakyThrows
    @Sql(scripts = {"/data/recipe.sql", "/data/picture.sql"})
    void getRecipeAll() {
        var perform = mockMvc.perform(get("/recipe").contentType(MediaType.APPLICATION_JSON)).andReturn();
        ObjectMapper objectMapper = new ObjectMapper().findAndRegisterModules();
        var list = objectMapper.readValue(perform.getResponse().getContentAsString(),
                new TypeReference<List<RecipeListDto>>() {
                });

        var recipeDto = new RecipeListDto(UUID.fromString("c80d54d3-500d-4539-9479-8e8961477193"),
                LocalDate.now(),
                "title", UUID.fromString("c80d54d3-500d-4539-9479-8e8961477193"));
        var recipeDto2 = new RecipeListDto(UUID.fromString("c80d54d3-500d-4539-9479-8e8961477194"),
                LocalDate.now(),
                "title2", null);
        assertThat(list).hasSize(2)
                .extracting(Object::toString)
                .containsExactlyInAnyOrder(recipeDto.toString(), recipeDto2.toString());
    }

    @Test
    @SneakyThrows
    @Sql({"/data/unit.sql", "/data/ingredient.sql", "/data/recipe.sql", "/data/measure.sql", "/data/picture.sql"})
    void getRecipeIngredientTypeContains() {
        var perform = mockMvc.perform(get("/recipe").param("typeContains", BEEF.name(), FISH.name())
                .contentType(MediaType.APPLICATION_JSON)).andReturn();
        ObjectMapper objectMapper = new ObjectMapper().findAndRegisterModules();
        var list = objectMapper.readValue(perform.getResponse().getContentAsString(),
                new TypeReference<List<RecipeListDto>>() {
                });

        var recipeDto = new RecipeListDto(UUID.fromString("c80d54d3-500d-4539-9479-8e8961477193"),
                LocalDate.now(),
                "title", UUID.fromString("c80d54d3-500d-4539-9479-8e8961477193"));
        assertThat(list).hasSize(1)
                .extracting(Object::toString)
                .containsExactlyInAnyOrder(recipeDto.toString());
    }

    @Test
    @SneakyThrows
    @Sql({"/data/unit.sql", "/data/ingredient.sql", "/data/recipe.sql", "/data/measure.sql", "/data/picture.sql"})
    void getRecipeIngredientTypeDoesNotContain() {
        var perform = mockMvc.perform(get("/recipe").param("typeDoesNotContain", BEEF.name())
                .contentType(MediaType.APPLICATION_JSON)).andReturn();
        ObjectMapper objectMapper = new ObjectMapper().findAndRegisterModules();
        var list = objectMapper.readValue(perform.getResponse().getContentAsString(),
                new TypeReference<List<RecipeListDto>>() {
                });

        var recipeDto = new RecipeListDto(UUID.fromString("c80d54d3-500d-4539-9479-8e8961477194"),
                LocalDate.now(),
                "title2", null);
        assertThat(list).hasSize(1)
                .extracting(Object::toString)
                .containsExactlyInAnyOrder(recipeDto.toString());
    }
}
