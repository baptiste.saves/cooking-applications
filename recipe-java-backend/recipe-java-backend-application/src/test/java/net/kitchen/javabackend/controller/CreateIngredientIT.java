package net.kitchen.javabackend.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import net.kitchen.javabackend.RecipeAppIT;
import net.kitchen.javabackend.dto.IngredientDto;
import net.kitchen.javabackend.dto.UnitDto;
import net.kitchen.javabackend.dto.create.CreateIngredientDto;
import net.kitchen.javabackend.dto.create.CreateUnitDto;
import net.kitchen.javabackend.dto.create.UpdateIngredientTypesDto;
import net.kitchen.javabackend.dto.create.UpdateIngredientUnitsDto;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Set;
import java.util.UUID;

import static net.kitchen.javabackend.PodamUtils.manufacturePojo;
import static net.kitchen.javabackend.dto.IngredientTypeEnum.BEEF;
import static net.kitchen.javabackend.dto.IngredientTypeEnum.PASTA;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RecipeAppIT
class CreateIngredientIT {
    @Autowired
    private MockMvc mockMvc;


    @Test
    @Sql({"/data/unit.sql", "/data/ingredient.sql"})
    @SneakyThrows
    void createUnit() {
        ObjectMapper objectMapper = new ObjectMapper();
        var createUnitDto = manufacturePojo(CreateUnitDto.class);
        var mvcResult = mockMvc.perform(post("/ingredient/unit")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(createUnitDto))
                )
                .andExpect(status().isCreated())
                .andReturn();

        var actual = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), UnitDto.class);
        assertThat(actual).returns(createUnitDto.getName(), UnitDto::getName);
        assertThat(actual.getId()).isNotNull();
    }

    @Test
    @Sql({"/data/unit.sql", "/data/ingredient.sql"})
    @SneakyThrows
    void createIngredients() {
        ObjectMapper objectMapper = new ObjectMapper();
        var createIngredientDto = manufacturePojo(CreateIngredientDto.class);
        var mvcResult = mockMvc.perform(post("/ingredient")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(createIngredientDto))
                )
                .andExpect(status().isCreated())
                .andReturn();

        var actual = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), IngredientDto.class);
        assertThat(actual).returns(createIngredientDto.getName(), IngredientDto::getName);
        assertThat(actual.getId()).isNotNull();
        assertThat(actual.getUnits()).isNullOrEmpty();
    }

    @Test
    @Sql({"/data/unit.sql", "/data/ingredient.sql"})
    @SneakyThrows
    void updateIngredientUnits() {
        var ingredientId = UUID.fromString("c80d54d3-500d-4539-9479-8e8961477193");

        ObjectMapper objectMapper = new ObjectMapper();
        var updateIngredientUnitsDto = new UpdateIngredientUnitsDto(Set.of(UUID.fromString(
                "c80d54d3-500d-4539-9479-8e8961477112")));
        var mvcResult = mockMvc.perform(put("/ingredient/{id}/units", ingredientId)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(updateIngredientUnitsDto))
                )
                .andExpect(status().isOk())
                .andReturn();

        var actual = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), IngredientDto.class);
        assertThat(actual.getId()).isNotNull();
        assertThat(actual.getUnits()).extracting(UnitDto::getName).containsExactlyInAnyOrder("Unit");
    }

    @Test
    @Sql({"/data/unit.sql", "/data/ingredient.sql"})
    @SneakyThrows
    void updateIngredientTypes() {
        var ingredientId = UUID.fromString("c80d54d3-500d-4539-9479-8e8961477193");

        ObjectMapper objectMapper = new ObjectMapper();
        var updateIngredientUnitsDto = new UpdateIngredientTypesDto(Set.of(BEEF, PASTA));
        var mvcResult = mockMvc.perform(put("/ingredient/{id}/type", ingredientId)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(updateIngredientUnitsDto))
                )
                .andExpect(status().isOk())
                .andReturn();

        var actual = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), IngredientDto.class);
        assertThat(actual.getId()).isNotNull();
        assertThat(actual.getTypes()).containsExactlyInAnyOrder(BEEF, PASTA);
    }
}
