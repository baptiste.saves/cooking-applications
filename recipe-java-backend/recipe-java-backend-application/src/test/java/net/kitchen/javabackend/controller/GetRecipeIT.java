package net.kitchen.javabackend.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import net.kitchen.javabackend.RecipeAppIT;
import net.kitchen.javabackend.dto.MeasureDto;
import net.kitchen.javabackend.dto.RecipeDto;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

@RecipeAppIT
class GetRecipeIT {

    @Autowired
    private MockMvc mockMvc;

    @Test
    @SneakyThrows
    @Sql(scripts = {"/data/recipe.sql", "/data/unit.sql", "/data/ingredient.sql", "/data/measure.sql", "/data/picture.sql"})
    void getRecipeNominalCase() {
        var perform = mockMvc.perform(get("/recipe/{id}", "c80d54d3-500d-4539-9479-8e8961477193")
                .contentType(MediaType.APPLICATION_JSON)).andReturn();
        ObjectMapper objectMapper = new ObjectMapper().findAndRegisterModules();
        var actual = objectMapper.readValue(perform.getResponse().getContentAsString(), RecipeDto.class);

        var recipeDto = RecipeDto.builder()
                .id(UUID.fromString("c80d54d3-500d-4539-9479-8e8961477193"))
                .pictureIds(List.of(UUID.fromString("c80d54d3-500d-4539-9479-8e8961477193")))
                .createdAt(LocalDate.now())
                .title("title")
                .content("new content")
                .yieldQuantity(3)
                .measures(
                        List.of(
                                MeasureDto.builder()
                                        .ingredient("creme")
                                        .unit("l")
                                        .quantity(10)
                                        .ingredientId(UUID.fromString("c80d54d3-500d-4539-9479-8e8961477193"))
                                        .unitId(UUID.fromString("c80d54d3-500d-4539-9479-8e8961477110"))
                                        .build(),
                                MeasureDto.builder()
                                        .ingredient("patate")
                                        .unit("g")
                                        .quantity(5)
                                        .ingredientId(UUID.fromString("c80d54d3-500d-4539-9479-8e8961477194"))
                                        .unitId(UUID.fromString("c80d54d3-500d-4539-9479-8e8961477111"))
                                        .build()
                        )
                );

        assertThat(actual)
                .usingRecursiveComparison()
                .isEqualTo(recipeDto);
    }
}
