# Context

This application is the main backend for the recipe features.

As it is the main backend of the application, it is built using a java 17, Spring boot, Hibernate combination
and a classical controller / service / persistence architecture

