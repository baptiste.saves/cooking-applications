# Context

This repo should contain a set of different applications created to manage recipes, inventory etc…

## Architecture

Front end in angular with 2 api server in java and python.

# Develop

## Run locally

* Export `TOKEN_GITLAB` as an env var
* `make login-registry`
* make run
* http://localhost:9999/recipe/list


## Java server

* Need `jdk 17` and `maven 3` installed
* Set $JAVA_HOME to point to jdk
* `mvn -version` should show java 17

## Python
* Install `poetry`
* Install `python 3.12`
* Create `venv`

## Front
* install `npm`


# Dev

Python and Angular clients are generated from open api docs but not dynamically at build time because automation was too
complicated

## Generate python client

> make generate-python-sdk

## Generate Angular client

See package.json: ng-openapi-gen-java, ng-openapi-gen-python